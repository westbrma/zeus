using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;

namespace Zeus
{
	[Route ("api/[controller]")]
	public class UserController : Controller
	{
		const string _cookieAuthScheme = CookieAuthenticationDefaults.AuthenticationScheme;

		[HttpGet ("LoginWithReset")]
		public IActionResult LoginWithReset (long id)
		{
			User user = null;

			user = Repo.Users.GetUserByResetID (id);
			if (user != null)
			{
				TryLoginIn (user.Email, null, user.PasswordHash);
			}

			return Redirect (HttpContext.GetRootURL ());
		}

		private SignInModel TryLoginIn (string username, string password, string passwordHash = null)
		{
			if (string.IsNullOrEmpty (passwordHash))
			{
				passwordHash = Helper.GetPasswordHash (password);
			}

			User user = Repo.Users.LoginUser (username, passwordHash);
			if (user != null)
			{
				var claims = new []
				{
					new Claim (ClaimTypes.Name, username),
						new Claim ("user_id", user.ID.ToString ())
				};
				var identity = new ClaimsIdentity (claims, _cookieAuthScheme);
				var pricipal = new ClaimsPrincipal (identity);
				HttpContext.SignInAsync (
					_cookieAuthScheme,
					pricipal,
					new AuthenticationProperties
					{
						IsPersistent = true
					}).Wait ();
				HttpContext.User = pricipal;

				return GetSignIn ();
			}
			else
			{
				throw new APIException ("Invalid Username or Password.", System.Net.HttpStatusCode.Forbidden);
			}
		}

		[HttpPost ("login")]
		public SignInModel Login ([FromBody] Login login) // string username, string password)
		{
			return TryLoginIn (login.Username, login.Password);
		}

		[HttpGet]
		public SignInModel GetSignIn ()
		{
			if (HttpContext.IsSignedIn ())
			{
				SignInModel result = new SignInModel ();
				result.Username = User.Email ();
				var user = User.UserObj ();
				result.Name = user.Name;
				result.ID = user.ID;
				result.IsAdmin = user.IsRole (UserRoles.Admin);
				result.Bookmarks = Repo.Providers.GetMyProviders (user.ID);
				return result;
			}

			return null;
		}

		[HttpPost ("Register")]
		public SignInModel Register ([FromBody] Login login)
		{
			if (Helper.IsValidEmail (login.Username))
			{
				User user = Repo.Users.RegisterUser (login.Username, login.Name, Helper.GetPasswordHash (login.Password));
				return TryLoginIn (login.Username, login.Password);
			}
			else
			{
				throw new APIException ("Invalid Email address.", System.Net.HttpStatusCode.BadRequest);
			}
		}

		[HttpPost ("SignOut")]
		public void SignOut ()
		{
			HttpContext.SignOutAsync (_cookieAuthScheme);
			if (HttpContext.Session != null)
			{
				HttpContext.Session.Clear ();
			}
			return;
		}

		[HttpPost ("ForgotPassword")]
		public void ForgotPassword ([FromBody] JObject data)
		{
			string email = data["email"].ToString ();
			string resetID = Repo.Users.CreateForgotPassword (email);
			if (!string.IsNullOrEmpty (resetID))
			{
				string recoverURL = HttpContext.GetRootApiURL ("user/loginWithReset?id=" + resetID);
				string body = "Click here: <a href=\"" + recoverURL + "\">" + recoverURL + "</a>";
				EmailHelper.SendEmail ("Password recovery", body, email, true);
			}
		}

		[APIAuthorize]
		[HttpDelete]
		public void DeleteAccount ()
		{
			int userID = User.UserID ();
			SignOut ();
			Repo.Users.DeleteAccount (userID);
		}

		[APIAuthorize]
		[HttpPut]
		public void UpdateAccount ([FromBody] Login login)
		{
			User user = User.UserObj ();
			if (user != null)
			{
				if (!Helper.IsValidEmail (login.Username))
				{
					throw new APIException ("Invalid Email address.", System.Net.HttpStatusCode.BadRequest);
				}
				else
				{
					if (user.Email != login.Username)
					{
						User other = Repo.Users.GetUserByEmail (login.Username);
						if (other != null)
						{
							throw new APIException ("Email address already registered.", System.Net.HttpStatusCode.BadRequest);
						}
						user.Email = login.Username;
					}

					user.PasswordHash = Helper.GetPasswordHash (login.Password);
					user.Name = login.Name;
					Repo.Users.UpdateUser (user, false);
				}
			}
		}
	}
}