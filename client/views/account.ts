@VueComponent({
	template: templates.fetch('account')
})
class Accounts extends Vue{
	account = state.account;
	username = '';
	password = '';
	name = '';
	confirmPassword = '';
	editMode = false;
	error = '';

	async beforeRouteEnter(to, from, next) {
		await state.signInPromise;
		next();
	}

	mounted(){
		this.clearForm();
	}

	toggleEditMode(){
		this.editMode = !this.editMode;
		if(!this.editMode){
			this.clearForm();
		}
	}

	clearForm(){
		this.username = state.account.username;
		this.name = state.account.name;
		this.error = '';
		this.editMode = false;
		this.password = '';
		this.confirmPassword = '';
	}

	saveChanges () {
		if(this.password == null || this.password == ''){
			this.error = 'Enter a new or existing password.';
			return;
		}

		if (this.password != this.confirmPassword) {
			this.error = 'Passwords do not match.';
			return;
		}

		var self = this;
		accountService.updateAccount(this.username, this.password, this.name).then(function(){
			self.clearForm();
			MessageDlg.open('Account successfully updated.');
		});
	}
}