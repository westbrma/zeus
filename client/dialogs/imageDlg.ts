@VueComponent({
	template: 
	`<div class="dialog" id="imageDlg">
		<img :src="imageUrl" />
		<div style="text-align:center">
			<button @click="close()">Close</button>
		</div>
	</div>`
})
class ImageDlg extends Vue {
	imageUrl = null;

	static open(url:string) {
		let vm = new ImageDlg();
		vm.imageUrl = url;
		dlg.open(vm);
	}

	close() {
		dlg.close(this);
	}
}