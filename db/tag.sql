USE [zeus]
GO

/****** Object:  Table [dbo].[tag]    Script Date: 5/8/2018 5:52:29 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[tag](
	[provider_id] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_tag] PRIMARY KEY CLUSTERED 
(
	[provider_id] ASC,
	[name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tag]  WITH CHECK ADD  CONSTRAINT [FK_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([id])
GO

ALTER TABLE [dbo].[tag] CHECK CONSTRAINT [FK_provider_id]
GO


