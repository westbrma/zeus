using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Zeus
{
	public class UserData
	{
		public List<User> GetAllUsers ()
		{
			List<User> result = new List<User> ();
			using (SqlConnection conn = Database.CreateConnection ())
			{
				using (SqlCommand cmd = new SqlCommand ("SELECT id, email, name, roles, last_login_date, create_date FROM [user]", conn))
				{

					using (IDataReader reader = cmd.ExecuteReader ())
					{
						while (reader.Read ())
						{
							result.Add (PopulateUser (new User (), reader, true));
						}
					}
				}
			}
			return result;
		}

		public User GetUser (int userID)
		{
			User user = null;
			using (SqlConnection conn = Database.CreateConnection ())
			{
				using (SqlCommand cmd = new SqlCommand ("SELECT id, email, name, roles, password_hash FROM [user] WHERE id = @ID", conn))
				{
					cmd.Parameters.AddWithValue ("@ID", userID.ToString ());

					using (IDataReader reader = cmd.ExecuteReader ())
					{
						if (reader.Read ())
						{
							user = new User ();
							PopulateUser (user, reader, false);
						}
					}
				}
			}
			return user;
		}

		private User PopulateUser (User user, IDataReader reader, bool forAdmin)
		{
			user.ID = reader.GetInt ("id");
			user.Email = reader.GetString ("email");
			user.Name = reader.GetString ("name");
			user.Roles = (UserRoles) reader["roles"];
			if (forAdmin)
			{
				user.LastLoginDate = reader.GetDateTime ("last_login_date").Value;
				user.CreateDate = reader.GetDateTime ("create_date").Value;
			}
			else
			{
				user.PasswordHash = reader.GetString ("password_hash");
			}
			return user;
		}

		public User LoginUser (string email, string passwordHash)
		{
			User user = GetUserByEmail (email);
			if (user != null && user.PasswordHash == passwordHash)
			{
				using (SqlConnection conn = Database.CreateConnection ())
				{
					using (SqlCommand cmd = new SqlCommand ("UPDATE [user] SET last_login_date = @LoginDate WHERE id = @ID", conn))
					{
						cmd.Parameters.AddWithValue ("@LoginDate", DateTime.UtcNow);
						cmd.Parameters.AddWithValue ("@ID", user.ID);

						cmd.ExecuteNonQuery ();
					}
				}

				return user;
			}

			return null;
		}

		public User RegisterUser (string email, string name, string passwordHash)
		{
			User newUser;
			User existingUser = GetUserByEmail (email);
			if (existingUser != null)
			{
				throw new APIException ("A user with that email already exists.", System.Net.HttpStatusCode.Conflict);
			}

			newUser = new User ();
			newUser.Email = email;
			newUser.Name = name;
			newUser.PasswordHash = passwordHash;
			newUser.Roles = UserRoles.User;
			UpdateUser (newUser, true);
			return newUser;
		}

		public User GetUserByEmail (string email)
		{
			if (string.IsNullOrEmpty (email))
			{
				return null;
			}

			User user = null;
			using (SqlConnection conn = Database.CreateConnection ())
			{
				using (SqlCommand cmd = new SqlCommand ("SELECT id, email, name, roles, password_hash FROM [user] WHERE email = @Email", conn))
				{
					cmd.Parameters.AddWithValue ("@Email", email);

					using (IDataReader reader = cmd.ExecuteReader ())
					{
						if (reader.Read ())
						{
							user = new User ();
							PopulateUser (user, reader, false);
						}
					}
				}
			}
			return user;
		}

		public void DeleteUser (int userID)
		{
			Database.DeleteWhere ("[user]", UserFields.ID, userID);
		}

		public void UpdateUser (User user, bool isNew)
		{
			using (SqlConnection conn = Database.CreateConnection ())
			{
				using (SqlCommand cmd = Database.CreateDbCmd (conn))
				{
					cmd.Parameters.AddWithValue ("@id", user.ID);
					cmd.Parameters.AddWithValue ("@password_hash", user.PasswordHash);
					cmd.Parameters.AddWithValue ("@email", user.Email);
					cmd.Parameters.AddWithValue ("@name", user.Name ?? string.Empty);
					cmd.Parameters.AddWithValue ("@roles", (int) user.Roles);

					if (isNew)
					{
						cmd.Parameters.AddWithValue ("@create_date", DateTime.UtcNow);
						cmd.Parameters.AddWithValue ("@last_login_date", DateTime.UtcNow);
					}

					cmd.SetInsertUpdateText ("[user]", isNew, user.ID, UserFields.ID);

					if (isNew)
					{
						user.ID = (int) cmd.ExecuteScalar ();
					}
					else
					{
						cmd.ExecuteNonQuery ();
					}
				}
			}
		}

		private PasswordReset GetPasswordReset (long ID)
		{
			PasswordReset reset = null;
			using (SqlConnection conn = Database.CreateConnection ())
			{
				using (SqlCommand cmd = Database.CreateDbCmd ("SELECT * FROM password_reset where id = @ID", conn))
				{
					cmd.Parameters.AddWithValue ("@ID", ID);

					using (IDataReader reader = cmd.ExecuteReader ())
					{
						if (reader.Read ())
						{
							reset = new PasswordReset ();
							reset.ID = reader.GetInt64 (0);
							reset.Email = reader.GetString (1);
							reset.SentTime = reader.GetDateTime (2);
						}
					}
				}
			}
			return reset;
		}

		public void DeleteAccount (int userID)
		{
			using (SqlConnection conn = Database.CreateConnection ())
			{
				using (SqlCommand cmd = Database.CreateDbCmd (conn))
				{
					cmd.CommandText = "DELETE FROM user WHERE id = @ID";
					cmd.Parameters.AddWithValue ("@ID", userID);

					cmd.ExecuteNonQuery ();
				}
			}
		}

		public string CreateForgotPassword (string email)
		{
			string result = null;
			User user = GetUserByEmail (email);
			if (user != null)
			{
				using (SqlConnection conn = Database.CreateConnection ())
				{
					using (SqlCommand cmd = Database.CreateDbCmd (conn))
					{
						long ID = Database.GetPrimaryKey ();
						cmd.Parameters.AddWithValue ("@id", ID);
						cmd.Parameters.AddWithValue ("@email", email);
						cmd.Parameters.AddWithValue ("@sent_time", DateTime.Now);
						cmd.SetInsertUpdateText ("password_reset", true, ID, "id");

						cmd.ExecuteNonQuery ();
						result = ID.ToString ();
					}
				}
			}
			return result;
		}

		public User GetUserByResetID (long resetID)
		{
			User user = null;
			PasswordReset reset = GetPasswordReset (resetID);
			if (reset != null)
			{
				TimeSpan span = DateTime.Now - reset.SentTime;
				if (span.TotalHours < 4)
				{
					user = GetUserByEmail (reset.Email);
				}
			}
			return user;
		}

		public void AddBookmark (int providerId, int userId)
		{
			using (SqlConnection conn = Database.CreateConnection ())
			{
				using (SqlCommand cmd = Database.CreateDbCmd ("bookmark_provider", conn))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.AddWithValue ("@pUserID", userId);
					cmd.Parameters.AddWithValue ("@pProviderID", providerId);

					cmd.ExecuteNonQuery ();
				}
			}
		}

		public void DeleteBookmark (int providerId, int userId)
		{
			using (SqlConnection conn = Database.CreateConnection ())
			{
				using (SqlCommand cmd = Database.CreateDbCmd ("delete_bookmark", conn))
				{
					cmd.CommandType = CommandType.StoredProcedure;
					cmd.Parameters.AddWithValue ("@pUserID", userId);
					cmd.Parameters.AddWithValue ("@pProviderID", providerId);

					cmd.ExecuteNonQuery ();
				}
			}
		}
	}
}