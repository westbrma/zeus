@VueComponent({
	props: ['html'],
	components: {
		'toolbox': Toolbox
	},
	template:
		`<div id="wysiwyg">
		<toolbox ref="tb" :editor="this" ></toolbox>
		<div id="quillDiv">
		</div>
		<input type="file" id="imgupload" style="display:none" accept=".png, .jpg, .jpeg" />
	</div>`
})
class WYSIWYG extends Vue {
	$div: JQuery<HTMLElement>;
	quill: Quill;
	activeTag = null;
	savedSelection = null;
	toolbox: Toolbox;
	changed: Boolean;

	mounted() {
		this.toolbox = <any>this.$refs['tb'];

		let div = document.getElementById('quillDiv');
		this.$div = $(div);

		let options: QuillOptions = {

		}

		this.quill = new Quill(div, options);

		$('#btImageSizeUp').click(() => {
			this.adjustImageSize(1.1);
		});

		$('#btImageSizeDown').click(() => {
			this.adjustImageSize(.9);
		});

		this.$div.on('click', 'img', () => {
			var img = $(this);
			img.addClass('selected');

			var offset = img.offset();
			this.toolbox.enabled = false;
			//imgDlg.show();
			//imgDlg.css('top', offset.top);
			//imgDlg.css('left', offset.left);
		});

		this.quill.on('text-change', (delta, oldDelta, source) => {
			this.setActiveTools();
			this.changed = true;
		});

		this.quill.on('selection-change', (range, oldRange, source) => {
			this.toolbox.enabled = range != null;
			if (range != null) {
				this.setActiveTools();
				//imgDlg.hide();
			}
		});
	}

	adjustImageSize(amount) {
		var img = $('img.selected');
		var width = img.width();
		img.attr('width', width * amount);
		this.changed = true;
	}

	setActiveTools() {
		if (!this.toolbox.enabled) {
			return;
		}

		var format = this.quill.getFormat();
		this.toolbox.tools.forEach(function (tool) {
			tool.setActive(this.activeTag, format);
		}, this);
	}

	setHTML(html: string) {
		//this.quill.setContents(<any>[{ insert: html }]);
		this.quill.clipboard.dangerouslyPasteHTML(html);
	}

	getHTML() {
		return this.quill.container.firstChild.innerHTML;
	}

	selectNode() {
		var my_node = this.quill.selection.getNativeRange().start.node;
		var range = document.createRange();
		range.selectNodeContents(my_node);
		var sel = window.getSelection();
		sel.removeAllRanges();
		sel.addRange(range);
	}

	saveSelection() {
		var range = this.quill.getSelection();
		this.savedSelection = range;
		if (range && range.length > 0) {
			return this.quill.getText(range.index, range.length);
		}
	}

	restoreSelection() {
		this.quill.setSelection(this.savedSelection);
	}

	getCaretIndex() {
		return this.quill.getSelection().index;
	}
}