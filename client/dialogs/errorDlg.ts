@VueComponent({
	template: templates.fetch('errorDlg')
})
class ErrorDlg extends Vue {
	errors = [];

	static open(errors:string[]) {
		let vm = new ErrorDlg();
		vm.errors = errors;
		dlg.open(vm);
	}

	close() {
		dlg.close(this);
	}
}