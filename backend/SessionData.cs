using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Zeus
{
	public class SessionData
	{
		ISession _session;

		public bool Authenticated
		{
			get
			{
				return _session.GetInt32 ("UserID").HasValue;
			}
		}

		public int UserID
		{
			get
			{
				return _session.GetInt32 ("UserID").Value;
			}
			set
			{
				_session.SetInt32 ("UserID", value);
			}
		}

		User _user;
		public User User
		{
			get
			{
				if (_user == null)
				{
					_user = Repo.Users.GetUser (UserID);
				}
				return _user;
			}
		}

		public SessionData (ISession session)
		{
			_session = session;
		}
	}

	public static class SessionHelper
	{
		public static SessionData GetSessionData (ISession session)
		{
			return new SessionData (session);
		}
	}
}