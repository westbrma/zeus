interface IInputHandler {
	isActive?: () => boolean;
	keydown?: (event?: JQuery.Event) => boolean | void;
	keyup?: (event?: JQuery.Event) => boolean | void;
}

class Dialog {
	firstInput: JQuery<HTMLElement> = null;
	lastInput: JQuery<HTMLElement> = null;

	$div: JQuery<HTMLElement> = null;
	zIndex: number = 0;
	onClose: Function = null;
	inputHandler: IInputHandler = null;
	canFocus = true;
	onActivate = null;

	activate() {
		dlg.activeDlg = this;
		input.setHandler(this.inputHandler);
		dlg.setOverlay(true, this);

		// setup first and last inputs
		var inputs = this.$div.find('select, input, textarea, button, a, button').filter(':visible');
		this.firstInput = inputs.first();
		this.lastInput = inputs.last();

		var self = this;

		function onLastInputKeydown(e: JQuery.Event) {
			if ((e.which === 9 && !e.shiftKey)) {
				e.preventDefault();
				self.firstInput.focus();
			}
		}

		function onFirstInputKeydown(e: JQuery.Event) {
			if ((e.which === 9 && e.shiftKey)) {
				e.preventDefault();
				self.lastInput.focus();
			}
		}

		/*redirect last tab to first input*/
		this.lastInput.off('keydown', onLastInputKeydown);
		this.lastInput.on('keydown', onLastInputKeydown);

		/*redirect first shift+tab to last input*/
		this.firstInput.off('keydown', onFirstInputKeydown);
		this.firstInput.on('keydown', onFirstInputKeydown);

		this.focus();

		if (this.onActivate) {
			this.onActivate();
		}
	}

	hide() {
		this.$div.hide();
		input.removeHandler(this.inputHandler);

		if (this.onClose != null) {
			this.onClose();
		}

		var next = dlg.topVisibleDialog();
		if (next != null) {
			next.activate();
		} else {
			dlg.setOverlay(false);
			input.setHandler(dlg.getDefaultHandler());
			dlg.activeDlg = null;
		}
	}

	show() {
		var topDlg = dlg.topVisibleDialog();
		if (topDlg != null) {
			this.zIndex = topDlg.zIndex + 2;
		} else this.zIndex = 1000;

		this.$div.css('z-index', this.zIndex);
		this.$div.show();
		this._centerOnScreen();
		this.center();
		dlg.registerDialog(this);
		this.activate();
	}

	center(){
		this._centerOnScreen();
		Vue.nextTick(()=> {
			this._centerOnScreen();
		})
	}

	_centerOnScreen() {
		let $div = this.$div;

		$div.css("left", 0);
		var top = ($(window).height() - $div.outerHeight()) / 2;
		var left = ($(window).width() - $div.outerWidth()) / 2;
		if (left < 0) {
			left = 0;
		}

		if (top >= 0) {
			$div.css('position', 'fixed');
			$div.css("top", top);
			$div.css("left", left);
		} else {
			top = 0;
			$div.css('position', 'absolute');
			$div.css("top", top + $(window).scrollTop());
			$div.css("left", left + $(window).scrollLeft());
		}
	}

	focus() {
		if (this.canFocus) {
			this.firstInput.focus();
		} else {
			(<HTMLElement>document.activeElement).blur();
		}
	}
}

class DialogManager {
	dlgList: Dialog[] = [];
	overlayEnabled:boolean = false;
	defaultHandler:IInputHandler = null;
	activeDlg: Dialog = null;

	constructor() {
		$(document).ready(() => {
			$(window).resize(() => {
				this.center();
			});
		});
	}

	registerDialog(dialog: Dialog) {
		var index = this.dlgList.indexOf(dialog);
		if (index >= 0) {
			this.dlgList.splice(index, 1);
		}
		this.dlgList.push(dialog);
	}

	setOverlay(enabled, dialog?: Dialog) {
		var overlay = $('#overlay');
		var scrollTop = 0;

		if (enabled) {
			overlay.css('z-index', dialog.zIndex - 1);

			if (!this.overlayEnabled) {
				overlay.addClass('active');

				if ($(document).height() > $(window).height()) {
					scrollTop = ($('html').scrollTop()) ? $('html').scrollTop() : $('body').scrollTop(); // Works for Chrome, Firefox, IE...
				}
				$('html').addClass('noscroll').css('top', -scrollTop);
			}

			this.overlayEnabled = true;
		} else {
			overlay.removeClass('active');

			scrollTop = parseInt($('html').css('top'));
			$('html').removeClass('noscroll');
			$('html,body').scrollTop(-scrollTop);
			this.overlayEnabled = false;
		}
	}

	center() {
		this.dlgList.forEach(d => {
			d.center();
		});
	}

	close(vm: Vue) {
		if (vm.dialog) {
			vm.dialog.hide();
		}

		vm.$destroy();
		vm.$el.remove();
	}

	open(vm: Vue, inputHandler?: IInputHandler): Dialog {
		var temp = document.createElement('div');
		document.body.appendChild(temp);
		var d = vm.dialog = new Dialog();
		d.inputHandler = inputHandler;
		vm.$mount(temp);
		d.$div = $(vm.$el);
		d.show();
		return d;
	}

	topVisibleDialog() {
		for (var i = this.dlgList.length - 1; i >= 0; i--) {
			var $dlg = this.dlgList[i].$div;
			if ($dlg.is(':visible') == true) {
				return this.dlgList[i];
			}
		}
		return null;
	}

	setDefaultHandler(handler: IInputHandler) {
		this.defaultHandler = handler;
		if (this.activeDlg == null) {
			input.setHandler(handler);
		}
	}

	getDefaultHandler(): IInputHandler {
		return this.defaultHandler;
	}
}

class InputHandler {
	handler: IInputHandler = null;
	skip = false;
	isKeyDown = false;

	constructor() {
		$(document).keydown(event => {
			this.isKeyDown = true;
			if (this.handler && this.handler.keydown) {
				return this.handler.keydown(event);
			}
		});

		$(document).keyup(event => {
			this.isKeyDown = false;
			if (!this.skip && this.handler && this.handler.keyup) {
				return this.handler.keyup(event);
			}
			this.skip = false;
		});
	}

	get active() {
		return this.handler != null;
	}

	setHandler(newHandler: IInputHandler) {
		this.handler = newHandler;
		this.skip = this.isKeyDown;
		if (document.activeElement) {
			(<any>document.activeElement).blur();
		}
	}

	removeHandler(oldHandler) {
		if (this.handler == oldHandler) {
			this.handler = null;
		}
	}
}

var dlg = new DialogManager();
var input = new InputHandler();