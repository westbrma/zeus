using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeus
{
	public static class DbExtensions
	{
		public static string GetString (this IDataReader reader, string column)
		{
			int col = reader.GetOrdinal (column);
			return reader.IsDBNull (col) ? null : reader.GetString (col);
		}

		public static bool GetBool (this IDataReader reader, string column)
		{
			int col = reader.GetOrdinal (column);
			return reader.GetBoolean (col);
		}

		public static int GetInt (this IDataReader reader, string column)
		{
			int col = reader.GetOrdinal (column);
			if (reader.IsDBNull (col))
			{
				return 0;
			}
			else
			{
				return reader.GetInt32 (col);
			}
		}

		public static int? GetNullInt (this IDataReader reader, string column)
		{
			int index = reader.GetOrdinal (column);
			if (reader.IsDBNull (index))
			{
				return null;
			}
			else
			{
				return reader.GetInt32 (index);
			}
		}

		public static DateTime? GetDateTime (this IDataReader reader, string column)
		{
			int col = reader.GetOrdinal (column);
			if (reader.IsDBNull (col))
			{
				return null;
			}
			else
			{
				return reader.GetDateTime (col);
			}
		}

		public static float GetFloat (this IDataReader reader, string column)
		{
			int col = reader.GetOrdinal (column);
			if (reader.IsDBNull (col))
			{
				return 0;
			}
			else
			{
				return (float) reader.GetDouble (col);
			}
		}

		public static void SetNullValues (this IDbCommand command)
		{
			foreach (IDataParameter param in command.Parameters)
			{
				if (param.Value == null)
				{
					param.Value = DBNull.Value;
				}
			}
		}

		public static void SetInsertUpdateText (this SqlCommand cmd, string table, bool isNew, long ID, string idColumn)
		{
			StringBuilder columns = new StringBuilder ();
			StringBuilder values = new StringBuilder ();
			bool foundIDParam = false;
			string idParamName = '@' + idColumn;

			foreach (IDbDataParameter param in cmd.Parameters)
			{
				if (param.ParameterName == idParamName)
				{
					foundIDParam = true;
					if (ID == 0 || !isNew)
					{
						continue;
					}
				}

				if (isNew)
				{
					columns.AppendFormat (", {0}", param.ParameterName.Replace ("@", ""));
					values.AppendFormat (", {0}", param.ParameterName);
				}
				else
				{
					values.AppendFormat (", {0} = {1}", param.ParameterName.Replace ("@", ""), param.ParameterName);
				}
			}

			// remove leading comma
			values.Remove (0, 1);

			if (isNew)
			{
				columns.Remove (0, 1);
				cmd.CommandText = string.Format ("INSERT INTO {0} ({1}) {2} VALUES ({3})",
					table,
					columns.ToString (),
					ID == 0 ? string.Format ("output INSERTED.{0}", idColumn) : string.Empty,
					values.ToString ());
			}
			else
			{
				cmd.CommandText = string.Format ("UPDATE {0} SET {1} WHERE {2} = {3}", table, values.ToString (), idColumn, idParamName);
				if (!foundIDParam)
				{
					cmd.Parameters.AddWithValue (idParamName, ID);
				}
			}
		}

		public static void AddParam (this SqlCommand cmd, string fieldConstant, object value)
		{
			cmd.Parameters.AddWithValue ("@" + fieldConstant, value);
		}
	}
}