USE [zeus]
GO

IF (OBJECT_ID('bookmark_provider') IS NOT NULL)
  DROP PROCEDURE [bookmark_provider]
GO

/****** Object:  StoredProcedure [dbo].[StarArticle]    Script Date: 8/20/2017 7:07:27 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[bookmark_provider]
	@pUserID	INT,
	@pProviderID	INT
AS
BEGIN

	SET NOCOUNT ON;

    SELECT 1 FROM alist WHERE user_id = @pUserID AND provider_id = @pProviderID
	IF @@ROWCOUNT = 0
	BEGIN
		DECLARE @bookmarks INT = 0
		
		INSERT INTO alist (provider_id, user_id) VALUES (@pProviderID, @pUserID)
		SELECT @bookmarks = COUNT(*) FROM alist WHERE provider_id = @pProviderID
		UPDATE provider SET referrals = @bookmarks WHERE id = @pProviderID
		RETURN @bookmarks
	END
END

GO


