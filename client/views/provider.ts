@VueComponent({
	template: templates.fetch('provider')
})
class ProviderView extends Vue {
	state = state;

	get provider(): Provider {
		return state.provider;
	}

	get profile(): Profile {
		return this.provider.profile;
	}

	viewImage(image){
		ImageDlg.open('/'+this.profile.folder+'/'+image);
	}

	beforeRouteEnter(to, from, next) {
		let id = parseInt(to.params.id);
		providerService.getProviderByID(id).then(response => {
			state.provider = response;
			next();
		});
	}
}