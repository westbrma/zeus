using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeus
{
	[Flags]
	public enum UserRoles
	{
		None = 0,
		User = 1,
		Admin = 4
	}

	public class User
	{
		public int ID;
		public string Email;
		public string Name;
		public string PasswordHash;
		public UserRoles Roles;
		public DateTime LastLoginDate;
		public DateTime CreateDate;

		public bool IsRole (UserRoles role)
		{
			return (Roles & role) > 0;
		}
	}

	public class PasswordReset
	{
		public long ID;
		public string Email;
		public DateTime SentTime;
	}
}