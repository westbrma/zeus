@VueComponent({
	template: templates.fetch('registerDlg')
})
class RegisterDlg extends Vue {
	username = '';
	name = '';
	password = '';
	confirmPassword = '';
	isOpen = false;
	error = '';

	static open() {
		let vm = new RegisterDlg();
		dlg.open(vm);
	}

	register() {
		if (this.password != this.confirmPassword) {
			this.error = 'Passwords do not match.';
			return;
		}

		accountService.register(this.username, this.password, this.name)
			.then(this.close)
			.catch(function (err) {

			});
	}

	close() {
		dlg.close(this);
	}
}