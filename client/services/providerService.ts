class ProviderService {
	getProfile(id: number): Promise<Profile> {
		return $http.get('/api/provider/profile' + id).then(r => {
			return r.data as Profile;
		});
	}

	getTags(search): Promise<string[]> {
		return $http.get('/api/provider/tags?search=' + search).then(function (response) {
			let tags: string[] = response.data;
			return tags;
		});
	}

	getProviderByID(providerId: number): Promise<Provider> {
		return $http.get('/api/provider/' + providerId).then(function (response) {
			let provider: Provider = response.data;
			provider.bookmarked = state.bookmarks.find(p => p.id == provider.id) != null;
			return provider;
		});
	}

	getProviders(search: string, sortBy?: SortBy): Promise<Provider[]> {
		var params = new URLSearchParams();
		var routeQuery: any = {};

		if (sortBy) {
			params.append("sortBy", sortBy);
		}

		if (search != null) {
			routeQuery.search = search;
			params.append("search", search);
		}

		params.append("xcoord", state.location.xcoord.toString());
		params.append("ycoord", state.location.ycoord.toString());

		app.setRouteQuery(routeQuery);

		return $http.get('/api/provider', {
			params: params
		}).then(function (response) {
			let providers: Provider[] = response.data;
			return providers;
		});
	}

	myProviders(): Promise<Provider[]> {
		return $http.get('/api/provider/mine').then(function (response) {
			let providers: Provider[] = response.data;
			return providers;
		});
	}

	createProvider(provider: Provider): Promise<any> {
		return $http.post('/api/provider', provider).then(function (response) {
			provider.id = response.data.value;
		}).then(()=> {
			this.bookmark(provider);
		});
	}

	saveProvider(provider: Provider) {
		return $http.put('/api/provider/' + provider.id, provider);
	}

	uploadImage(providerId:number, file:File){
		const data = new FormData();
		data.append("image", file);
		return axios.post('/api/provider/image/'+providerId, data, {
			headers: {
			  'Content-Type': 'multipart/form-data'
			}
	  });
	}

	deleteProvider(articleId) {
		return $http.delete('/api/provider/' + articleId);
	}

	bookmark(provider: Provider) {
		return $http.post('/api/provider/bookmark/' + provider.id, null).then(()=>{
			provider.bookmarked = true;
			provider.referrals++;
			state.bookmarks.push(provider);
		})
	}

	deleteBookmark(provider : Provider){
		return $http.delete('/api/provider/bookmark/' + provider.id).then(()=>{
			provider.bookmarked = false;
			provider.referrals--;
			state.bookmarks.forEach(p => {
				if(p.id == provider.id){
					removeFromArray(p, state.bookmarks);
				}
			});
		})
	}
}

var providerService = new ProviderService();