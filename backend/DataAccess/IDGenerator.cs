using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeus
{
	public static class IDGenerator
	{
		const string chars = "abcdefghijklmnopqrstuvwxyz0123456789";
		static Random random = new Random ();
		static char[] stringChars = new char[8];

		public static string GenerateID ()
		{
			for (int i = 0; i < stringChars.Length; i++)
			{
				stringChars[i] = chars[random.Next (chars.Length)];
			}

			return new String (stringChars);
		}
	}
}