using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Zeus.Controllers
{
	[APIAuthorize (UserRoles.Admin)]
	[Route ("api/[controller]")]
	public class AdminController : Controller
	{
		[HttpGet ("Users")]
		public List<User> GetUsers ()
		{
			return Repo.Users.GetAllUsers ();
		}
	}
}