@VueComponent({
	template: `
	<div class="dialog">
		<div>{{message}}</div>
		<div class="footer">
			<button @click="close()">OK</button>
		</div>
	</div>`
})
class MessageDlg extends Vue {
	message:string = '';

	static open(message:string){
		let vm = new MessageDlg();
		vm.message = message;
		dlg.open(vm);
	}

	close(){
		dlg.close(this);
	}
}