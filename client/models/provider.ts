class Provider {
	id:number = 0;
	ownerId:number = 0;
	name:string = '';
	desc:string = '';
	tags:string[] = [];
	city:string = '';
	state:string = '';
	zip:string = '';
	phone:string = '';
	website:string = '';
	referrals:number = 0;
	xCoord:number = 0;
	yCoord:number = 0;
	profile:Profile = null;

	changed:boolean = true;
	bookmarked:boolean = false;
}