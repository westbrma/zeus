using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeus
{
	public enum Availability
	{
		High,
		Medium,
		Low
	}

	public class Provider
	{
		public int ID;
		public string Name;
		public List<string> Tags;
		public string Website;
		public string Phone;
		public string City;
		public string State;
		public string Zip;
		public float XCoord;
		public float YCoord;
		public Availability Availability;
		public int Referrals;
		public int? OwnerId;
		public Profile Profile;
	}
}