using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeus
{
	static class UserFields
	{
		public const string ID = "id";
		public const string Email = "email";
		public const string PasswordHash = "password_hash";
		public const string Name = "name";
		public const string LastLoginDate = "last_login_date";
	}

	static class AListFields
	{
		public const string Table = "alist";
		public const string ProviderID = "provider_id";
		public const string UserID = "user_id";
	}

	static class ImageFields
	{
		public const string Table = "image";
		public const string ProviderID = "provider_id";
		public const string ImageID = "image_id";
	}

	static class TagFields
	{
		public const string Table = "tag";
		public const string ProviderID = "provider_id";
		public const string Name = "name";
	}

	static class ProfileFields
	{
		public const string Table = "profile";
		public const string ID = "profile_id";
		public const string ProviderID = "provider_id";
		public const string Folder = "folder";
		public const string Html = "html";
		public const string BgColor = "bg_color";
		public const string FontColor = "font_color";
		public const string Banner = "banner";
		public const string Images = "images";
	}

	static class ProviderFields
	{
		public const string Table = "provider";
		public const string ID = "id";
		public const string Name = "name";
		public const string Address = "address";
		public const string City = "city";
		public const string State = "state";
		public const string Zip = "zip";
		public const string Website = "website";
		public const string Phone = "phone";
		public const string YCoord = "ycoord";
		public const string XCoord = "xcoord";
		public const string UserID = "user_id";
		public const string Referrals = "referrals";
		public const string CreateDate = "create_date";
	}
}