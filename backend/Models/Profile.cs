using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeus
{
	public class Profile
	{
		public int ID;
		public List<string> Images;
		public string Html;
		public string BgColor;
		public string font;
		public string fontSize;
		public string FontColor;
		public string Banner;
		public string Folder;
	}
}