import { AxiosStatic, AxiosRequestConfig } from 'axios'
import Vue2 from 'vue'
import VueRouter2, { RouteConfig as RouteConfig2, Route } from 'vue-router'
import Quill2, { QuillOptionsStatic } from 'quill'

declare global {
	var axios:AxiosStatic;

	interface AxiosConfig extends AxiosRequestConfig {

	}

	class VueRouter extends VueRouter2 {

	}

	class Vue extends Vue2 {
		dialog:Dialog;
		$route:Route;
		$router:VueRouter
	}

	interface RouteConfig extends RouteConfig2 {

	}

	interface QuillOptions extends QuillOptionsStatic {
		
	}

	class Quill extends Quill2 {
		history:any;
		container:any;
		selection:any;
	}
}