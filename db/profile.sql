USE [zeus]
GO

/****** Object:  Table [dbo].[profile]    Script Date: 5/22/2018 5:04:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[profile](
	[profile_id] [int] IDENTITY(1,1) NOT NULL,
	[provider_id] [int] NOT NULL,
	[folder] varchar(50) NOT NULL,
	[html] [varchar](max) NULL,
	[bg_color] [varchar](20) NULL,
	[banner] [varchar](50) NULL,
	[font_color] [varchar](20) NULL,
	[images] [varchar](max) NULL
 CONSTRAINT [PK_profile] PRIMARY KEY CLUSTERED 
(
	[profile_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[profile]  WITH CHECK ADD  CONSTRAINT [FK_profile_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([id])
GO

ALTER TABLE [dbo].[profile] CHECK CONSTRAINT [FK_profile_provider_id]
GO


