class UserLocation {
	address: string = null;
	city: string = null;
	state: string = null;
	zip: string = null;
	xcoord: number = 0;
	ycoord: number = 0;
}