using System.Net;
using System.Net.Mail;

namespace Zeus
{
	public static class EmailHelper
	{
		public static void SendEmail (string subject, string message, string email, bool html = false)
		{
			MailMessage mailMessage = new MailMessage ();
			mailMessage.From = new MailAddress ("postmaster@stagepen.com");
			mailMessage.To.Add (email);
			mailMessage.Body = message;
			mailMessage.Subject = subject;
			mailMessage.IsBodyHtml = html;

			var server = "mail.stagepen.com";
			var port = 8889;
			using (SmtpClient client = new SmtpClient (server, port))
			{
				client.UseDefaultCredentials = false;
				client.Credentials = new NetworkCredential("postmaster@stagepen.com", "P@ssw0rd");
				client.Send (mailMessage);
			}
		}
	}
}