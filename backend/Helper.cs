using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Zeus
{
	public static class Helper
	{
		public static bool IsDebug = true;

		public static string GetPasswordHash (string password)
		{
			if (string.IsNullOrEmpty (password))
			{
				return string.Empty;
			}

			return Convert.ToBase64String (System.Security.Cryptography.MD5.Create ().ComputeHash (Encoding.UTF8.GetBytes (password)));
		}

		public static bool IsValidEmail (string email)
		{
			if (string.IsNullOrEmpty (email))
			{
				return false;
			}
			// Return true if strIn is in valid e-mail format.
			return Regex.IsMatch (email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
		}
	}
}