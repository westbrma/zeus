accountService.getLogin();
locationService.fetch();

@VueComponent({
	router: router
})
class App extends Vue {
	eventBus = new Vue();

	get route() {
		return this.$router.currentRoute;
	}

	get routeParams() {
		return this.route ? this.route.params : null;
	}

	fireEvent(arg1?, arg2?, arg3?, arg4?, arg5?) {
		this.eventBus.$emit.apply(this.eventBus, arguments);
	}

	goto(route:string, params?) {
		if (params) {
			this.$router.push({
				path: route,
				query: params
			})
		} else {
			this.$router.push(route);
		}
	}

	setRouteQuery(query) {
		var cur = {
			path: this.$router.currentRoute.path,
			params: this.$router.currentRoute.params,
			query: query
		}

		this.$router.replace(cur)
	}

	atRoute(route:string) {
		return this.route.path.toLowerCase() === route.toLowerCase();
	}

	reload() {
		location.reload();
	}
}

var app = new App();

window.onerror = function (message: string, filename?: string, lineno?: number, colno?: number, error?: Error) {
	if (error) {
		ErrorDlg.open([error.stack]);
	} else if (message) {
		ErrorDlg.open([message]);
	}
};

window.addEventListener("unhandledrejection", (e: PromiseRejectionEvent) => {
	if (e.reason && e.reason.message) {
		//ErrorDlg.open(e.reason.message);
	}
});

$(function () {
	app.$mount("#app");
});