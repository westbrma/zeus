using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Zeus
{
	public class BaseController : Controller
	{
		public int UserID
		{
			get
			{
				return User.UserID ();
			}
		}

		public string GetFilePath (string folder, string filename)
		{
			return Path.Combine (GetFolderPath (folder), filename);
		}

		public string GetFolderPath (string folder)
		{
			var path = Path.Combine (Directory.GetCurrentDirectory (), "wwwroot", folder);
			DirectoryInfo di = Directory.CreateDirectory (path);
			return path;
		}
	}
}