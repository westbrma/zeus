@VueComponent({
	template: templates.fetch('providerBar')
})
class ProviderBar extends Vue {
	app = app;
	state = state;
	provider = state.provider;

	edit(){
		app.goto(Pages.EditProvider+'/'+state.provider.id);
	}

	bookmark(){
		providerService.bookmark(this.provider);
	}

	deleteBookmark(){
		providerService.deleteBookmark(this.provider);
	}
}