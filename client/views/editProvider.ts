@VueComponent({
	components: {
		'wysiwyg':WYSIWYG
	},
	template: templates.fetch('editProvider')
})
class EditProvider extends Vue {
	provider: Provider = null;
	states = states;
	amOwner: boolean = false;
	isNew: boolean = false;
	editor:WYSIWYG;

	async beforeRouteEnter(to, from, next) {
		let id = parseInt(to.params.id);
		if (id) {
			state.provider = await providerService.getProviderByID(id);
		} else {
			state.provider = new Provider();
			state.provider.profile = new Profile();
		}

		next();
	}

	get profile(): Profile {
		return this.provider.profile;
	}

	addImage() {
		let fileUpload: HTMLInputElement = $('#tbImage')[0] as HTMLInputElement;
		fileUpload.click();
		fileUpload.onchange = this.handleFiles;
	}

	removeImage(image:string){
		removeFromArray(image, this.profile.images);
	}

	async handleFiles(e:Event) {
		let input:HTMLInputElement = e.target as HTMLInputElement;
		if (input.files && input.files.length == 1) {
			var file = input.files[0];
			await providerService.uploadImage(state.provider.id, file);
			this.profile.images.push(file.name);
		}
	}

	created() {
		this.provider = state.provider;
		this.isNew = this.provider.id == 0;
		this.amOwner = this.provider.ownerId == state.account.id;

		app.eventBus.$on('cancel-edit', this.close);
		app.eventBus.$on('save-provider', this.save);
		app.eventBus.$on('delete-article', this.deleteProvider);
	}

	mounted(){
		if(!this.profile.html){
			this.profile.html = '<div>hello <b>world</b></div>';
		}
		this.editor = <any>this.$refs['editor'];
		this.editor.setHTML(this.profile.html);
		$(this.$el).find('input:first').focus();
	}

	destroyed() {
		app.eventBus.$off('cancel-edit', this.close);
		app.eventBus.$off('save-provider', this.save);
		app.eventBus.$off('delete-article', this.deleteProvider);
	}

	async deleteProvider() {
		let response = await ConfirmDlg.open('Are you sure you want to delete this listing?')
		if (response) {
			providerService.deleteProvider(this.provider.id).then(function () {
				app.goto(Pages.Home);
			});
		}
	}

	async save() {
		if(!this.provider.name || !this.provider.city || !this.provider.state || !this.provider.tags.length){
			ErrorDlg.open(['Name, Tags, City, and State are required fields']);
			return;
		}

		let loc = new UserLocation();
		loc.city = this.provider.city;
		loc.state = this.provider.state;
		loc.zip = this.provider.zip;
		await locationService.setCoords(loc);
		this.provider.xCoord = loc.xcoord;
		this.provider.yCoord = loc.ycoord;

		this.profile.html = this.editor.getHTML();

		if (this.amOwner) {
			this.provider.ownerId = state.account.id;
		} else {
			this.provider.ownerId = null;
		}

		if (this.isNew) {
			providerService.createProvider(this.provider).then(() => {
				this.close();
			});
		} else {
			providerService.saveProvider(this.provider).then(() => {
				this.close();
			});
		}
	}

	close() {
		if(this.provider.id == 0){
			app.goto(Pages.Home);
		}else{
			app.goto(Pages.Provider + '/' + this.provider.id);
		}
	}
}