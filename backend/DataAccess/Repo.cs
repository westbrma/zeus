using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeus
{
	public static class Repo
	{
		public static ProviderData Providers { get; private set; }
		public static UserData Users { get; private set; }

		static Repo ()
		{
			Users = new UserData ();
			Providers = new ProviderData ();
		}
	}
}