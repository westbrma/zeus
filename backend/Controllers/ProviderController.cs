using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Zeus
{
	[Route ("api/[controller]")]
	public class ProviderController : BaseController
	{
		[HttpGet]
		public List<Provider> GetProviders (string search, float xcoord, float ycoord, SortBy sortBy, int? page, int? perPage)
		{
			return Repo.Providers.GetProviders (search, xcoord, ycoord, sortBy, page, perPage);
		}

		[HttpGet ("{id}")]
		public Provider GetProvider (int id)
		{
			var result = Repo.Providers.GetProviderById (id);
			return result;
		}

		[APIAuthorize]
		[HttpPost ("bookmark/{id}")]
		public void Bookmark (int id)
		{
			Repo.Users.AddBookmark (id, UserID);
		}

		[APIAuthorize]
		[HttpDelete ("bookmark/{id}")]
		public void DeleteBookmark (int id)
		{
			Repo.Users.DeleteBookmark (id, UserID);
		}

		[APIAuthorize]
		[HttpPost ("comment/{id}")]
		public void Comment (int id)
		{

		}

		[APIAuthorize]
		[HttpPost]
		public GeneralResult CreateProvider ([FromBody] Provider provider)
		{
			return SaveProvider (provider, 0);
		}

		[APIAuthorize]
		[HttpPut ("{id}")]
		public GeneralResult SaveProvider ([FromBody] Provider provider, int id)
		{
			if (id > 0)
			{
				Repo.Providers.VerifyOwner (UserID, id);
			}

			GeneralResult result = new GeneralResult ();
			if (string.IsNullOrEmpty (provider.Name))
			{
				result.AddError ("Name Required");
				Response.StatusCode = StatusCodes.Status400BadRequest;
			}

			if (!result.HasErrors)
			{
				Repo.Providers.SaveProvider (provider);
				result.Success = true;
				result.Value = provider.ID;
			}

			return result;
		}

		[APIAuthorize]
		[HttpPost ("image/{id}")]
		public async void UploadImage (IFormFile image, int id)
		{
			Repo.Providers.VerifyOwner (UserID, id);
			var profile = Repo.Providers.GetProfile (id);
			string filePath = GetFilePath (profile.Folder, image.FileName);

			if (image == null || image.Length == 0)
			{
				throw new APIException ("file not selected");
			}

			using (var stream = new FileStream (filePath, FileMode.Create))
			{
				await image.CopyToAsync (stream);
			}
		}

		[APIAuthorize]
		[HttpDelete ("{id}")]
		public void DeleteProvider (int id)
		{
			Repo.Providers.VerifyOwner (UserID, id);
			Repo.Providers.DeleteProvider (id);
		}

		[HttpGet ("tags")]
		public IEnumerable<string> GetTags (string search)
		{
			return Repo.Providers.GetTags (search);
		}
	}
}