USE [zeus]
GO

IF (OBJECT_ID('delete_bookmark') IS NOT NULL)
  DROP PROCEDURE [delete_bookmark]
GO

/****** Object:  StoredProcedure [dbo].[StarArticle]    Script Date: 8/20/2017 7:07:27 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[delete_bookmark]
	@pUserID	INT,
	@pProviderID	INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @bookmarks INT = 0
		
	DELETE FROM alist WHERE provider_id = @pProviderID AND user_id = @pUserID
	SELECT @bookmarks = COUNT(*) FROM alist WHERE provider_id = @pProviderID
	UPDATE provider SET referrals = @bookmarks WHERE id = @pProviderID
	RETURN @bookmarks
END

GO


