using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeus
{
	public class GeneralResult
	{
		public bool Success { get; set; }
		public List<string> Errors { get; set; }
		public object Value { get; set; }

		public string Error
		{
			set
			{
				AddError (value);
			}
		}

		public bool HasErrors
		{
			get
			{
				return Errors != null && Errors.Count > 0;
			}
		}

		public void AddError (string error)
		{
			if (Errors == null)
			{
				Errors = new List<string> ();
			}

			Errors.Add (error);
		}
	}
}