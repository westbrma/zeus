using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Zeus
{
	public class ExceptionMiddleware
	{
		private readonly RequestDelegate _next;
		static JsonSerializerSettings _jsonSettings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver () };

		public ExceptionMiddleware (RequestDelegate next)
		{
			_next = next;
		}

		public async Task Invoke (HttpContext context)
		{
			try
			{
				await _next (context);
			}
			catch (Exception ex)
			{
				GeneralResult result = new GeneralResult ();
				result.AddError (ex.Message);
				context.Response.ContentType = new MediaTypeHeaderValue ("application/json").ToString ();

				if (ex is APIException)
				{
					context.Response.StatusCode = ((APIException) ex).StatusCode;
				}
				else
				{
					context.Response.StatusCode = (int) HttpStatusCode.InternalServerError;
				}

				await context.Response.WriteAsync (JsonConvert.SerializeObject (result, Formatting.None, _jsonSettings), Encoding.UTF8);
			}
		}
	}
}