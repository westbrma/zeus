using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace Zeus
{
	public class Startup
	{
		IHostingEnvironment _env { get; set; }

		public Startup (IHostingEnvironment env)
		{
			_env = env;
		}

		public void ConfigureServices (IServiceCollection services)
		{
			services.AddMvc ();

			// Adds a default in-memory implementation of IDistributedCache.
			services.AddDistributedMemoryCache ();

			services.AddSession (options =>
			{
				options.Cookie.HttpOnly = true;
			});

			services.AddAuthentication (CookieAuthenticationDefaults.AuthenticationScheme).AddCookie ();

			string pathToCryptoKeys = Path.Combine (_env.ContentRootPath, "dp_keys");
			services.AddDataProtection ()
				.PersistKeysToFileSystem (new System.IO.DirectoryInfo (pathToCryptoKeys))
				.SetDefaultKeyLifetime (TimeSpan.FromDays (6000));
		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure (IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
		{
			loggerFactory.AddConsole ();

			if (env.IsDevelopment ())
			{
				//app.UseDeveloperExceptionPage();
				Helper.IsDebug = true;
			}
			else
			{
				Helper.IsDebug = false;
			}

			app.UseSession ();
			app.UseAuthentication ();
			app.UseStaticFiles ();
			app.UseMiddleware<ExceptionMiddleware> ();

			app.UseMvc (routes =>
			{
				routes.MapSpaFallbackRoute (
					name: "spa-fallback",
					defaults : new { controller = "Home", action = "Index" });
			});

			/*app.MapWhen(x => !x.Request.Path.Value.StartsWith("/api"), builder =>
			{
				builder.UseMvc(routes =>
				{
					routes.MapSpaFallbackRoute(
						name: "spa-fallback",
						defaults: new { controller = "Home", action = "Index" });
				});
			});*/
		}
	}
}