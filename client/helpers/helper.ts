function removeFromArray(item:any, array:any[]) {
	var idx = array.indexOf(item);
	if (idx >= 0) {
		array.splice(idx, 1);
	}
}

function cycleNext<T>(current:T, array:T[]):T{
	var idx = array.indexOf(current);
	idx++;
	if(idx >= array.length){
		idx = 0;
	}

	return array[idx];
}

function shuffleArray(a:any[]) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

function lerp(value1, value2, amount) {
	amount = amount < 0 ? 0 : amount;
	amount = amount > 1 ? 1 : amount;
	return value1 + (value2 - value1) * amount;
}

function getRandomMapItem<T>(obj) {
    var result;
    var count = 0;
    for (var prop in obj)
        if (Math.random() < 1/++count)
           result = prop;
    return result;
}

function getRandomItem<T>(arg: T[]): T {
	if (arg.length == 0) return null;
	return arg[getRandomInt(0, arg.length - 1)];
}

function getRandomInt(min, max):number {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

function getRandomChance(chance: number): boolean {
	if (chance <= 0 || chance >= 1) {
		throw new Error("Invalid RandomChance parameter value");
	}

	return Math.random() <= chance;
}

function getRandom(min, max):number {
	return Math.random() * (max - min) + min;
}

function clearSelection() {
	if (window.getSelection) {
		if (window.getSelection().empty) {  // Chrome
			window.getSelection().empty();
		} else if (window.getSelection().removeAllRanges) {  // Firefox
			window.getSelection().removeAllRanges();
		}
	}
}

function makeid() {
	var text = "";
	var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

	for (var i = 0; i < 5; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}