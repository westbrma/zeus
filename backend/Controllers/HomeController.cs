using Microsoft.AspNetCore.Mvc;

namespace Zeus.Controllers
{
	public class HomeController : Controller
	{
		[HttpGet]
		public IActionResult Index ()
		{
			return File ("~/index.html", "text/html");
		}
	}
}