using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Zeus
{
	public static class HttpContextExt
	{
		public static bool IsSignedIn (this HttpContext context)
		{
			return context.User.Identity.IsAuthenticated;
		}

		public static string GetRootURL (this HttpContext context)
		{
			string host = context.Request.Host.Host;
			if (host == "localhost")
			{
				host += ":" + context.Request.Host.Port;
			}
			string result = "http://" + host;
			return result;
		}

		public static string GetRootApiURL (this HttpContext context, string postfix)
		{
			return context.GetRootURL () + "/api/" + postfix;
		}
	}
}