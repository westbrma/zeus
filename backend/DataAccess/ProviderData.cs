using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Zeus
{
	public enum SortBy
	{
		Alist,
		Stars
	}

	public class ProviderData
	{
		HashSet<string> _tagHash = new HashSet<string> ();
		List<string> _tags = new List<string> ();

		public ProviderData ()
		{
			LoadTags ();
		}

		public IEnumerable<string> GetTags (string search)
		{
			if (string.IsNullOrEmpty (search))
			{
				return _tags;
			}

			search = search.ToLower ();
			return _tags.Where (t => t.StartsWith (search));
		}

		public void VerifyOwner (int userId, int providerId)
		{
			using (SqlConnection conn = Database.CreateConnection ())
			{
				using (SqlCommand cmd = Database.CreateDbCmd (conn))
				{
					cmd.CommandText = "SELECT user_id FROM provider WHERE id = @ID";
					cmd.Parameters.AddWithValue ("@ID", providerId);

					using (var reader = cmd.ExecuteReader ())
					{
						if (reader.Read ())
						{
							var owner = reader.GetInt (ProviderFields.UserID);
							if (owner > 0 && owner != userId)
							{
								throw new APIException ("You do not own this provider.", System.Net.HttpStatusCode.BadGateway);
							}
						}
						else
						{
							throw new APIException ("This provider does not exist", System.Net.HttpStatusCode.BadGateway);
						}
					}
				}
			}
		}

		private void LoadTags ()
		{
			var defaults = new string[] { "electrican", "cleaner", "handyman", "flooring", "windows", "doors" };
			foreach (var tag in defaults)
			{
				cacheTag (tag);
			}

			using (SqlConnection conn = Database.CreateConnection ())
			{
				using (SqlCommand cmd = Database.CreateDbCmd (conn))
				{
					cmd.CommandText = "SELECT DISTINCT name FROM tag";

					using (var reader = cmd.ExecuteReader ())
					{
						while (reader.Read ())
						{
							cacheTag (reader.GetString ("name"));
						}
					}
				}
			}

			_tags.Sort ();
		}

		private void cacheTag (string tag)
		{
			tag = tag.ToLower ();

			if (!_tagHash.Contains (tag))
			{
				_tags.Add (tag);
				_tagHash.Add (tag);
			}
		}

		private string TitleCase (string str)
		{
			str = str.ToLower ().Trim ();
			if (str.IndexOf (' ') >= 0)
			{
				var list = str.Split (' ');
				for (var i = 0; i < list.Length; i++)
				{
					list[i] = char.ToUpper (list[i][0]) + list[i].Substring (1);
				}

				return string.Join (" ", list);
			}
			else
			{
				return char.ToUpper (str[0]) + str.Substring (1);
			}
		}

		public void SaveProvider (Provider provider)
		{
			bool isNew = provider.ID == 0;
			using (SqlConnection conn = Database.CreateConnection ())
			{
				using (SqlCommand cmd = Database.CreateDbCmd (conn))
				{
					cmd.AddParam (ProviderFields.ID, provider.ID);
					cmd.AddParam (ProviderFields.UserID, provider.OwnerId);
					cmd.AddParam (ProviderFields.Name, provider.Name);
					cmd.AddParam (ProviderFields.City, provider.City);
					cmd.AddParam (ProviderFields.State, provider.State);
					cmd.AddParam (ProviderFields.Zip, provider.Zip);
					cmd.AddParam (ProviderFields.Website, provider.Website);
					cmd.AddParam (ProviderFields.Phone, provider.Phone);
					cmd.AddParam (ProviderFields.XCoord, provider.XCoord);
					cmd.AddParam (ProviderFields.YCoord, provider.YCoord);
					cmd.AddParam (ProviderFields.Referrals, provider.Referrals);

					if (isNew)
					{
						cmd.AddParam (ProviderFields.CreateDate, DateTime.UtcNow);
					}

					cmd.SetInsertUpdateText (ProviderFields.Table, isNew, provider.ID, ProviderFields.ID);
					cmd.SetNullValues ();

					if (isNew)
					{
						provider.ID = (int) cmd.ExecuteScalar ();
					}
					else
					{
						cmd.ExecuteNonQuery ();
					}

					SaveTags (provider, conn);
				}

				if (isNew && provider.Profile == null)
				{
					provider.Profile = new Profile ();
				}

				if (provider.Profile != null)
				{
					SaveProfile (provider.ID, provider.Profile, conn);
				}
			}
		}

		private void SaveProfile (int providerID, Profile profile, SqlConnection conn)
		{
			string images = string.Join (';', profile.Images);

			if (String.IsNullOrEmpty (profile.Folder))
			{
				profile.Folder = "Profiles/" + IDGenerator.GenerateID ();
			}

			bool isNew = profile.ID == 0;
			using (SqlCommand cmd = Database.CreateDbCmd (conn))
			{
				cmd.AddParam (ProfileFields.ID, profile.ID);
				cmd.AddParam (ProfileFields.ProviderID, providerID);
				cmd.AddParam (ProfileFields.Folder, profile.Folder);
				cmd.AddParam (ProfileFields.Html, profile.Html);
				cmd.AddParam (ProfileFields.Banner, profile.Banner);
				cmd.AddParam (ProfileFields.BgColor, profile.BgColor);
				cmd.AddParam (ProfileFields.FontColor, profile.FontColor);
				cmd.AddParam (ProfileFields.Images, images);

				cmd.SetInsertUpdateText (ProfileFields.Table, isNew, profile.ID, ProfileFields.ID);
				cmd.SetNullValues ();

				if (isNew)
				{
					profile.ID = (int) cmd.ExecuteScalar ();
				}
				else
				{
					cmd.ExecuteNonQuery ();
				}
			}
		}

		public void SaveTags (Provider provider, SqlConnection conn)
		{
			using (SqlCommand cmd = Database.CreateDbCmd (conn))
			{
				cmd.CommandText = "DELETE FROM tag WHERE provider_id = @provider_id";
				cmd.Parameters.AddWithValue ("@provider_id", provider.ID);
				cmd.ExecuteNonQuery ();
			}

			var tags = provider.Tags.ToList ();
			tags.Add (provider.Name);
			foreach (var tag in provider.Tags)
			{
				cacheTag (tag);
				using (SqlCommand cmd = Database.CreateDbCmd (conn))
				{
					cmd.CommandText = "INSERT INTO tag (provider_id, name) VALUES (@provider_id, @name)";
					cmd.Parameters.AddWithValue ("@provider_id", provider.ID);
					cmd.Parameters.AddWithValue ("@name", tag.ToLower ());
					cmd.ExecuteNonQuery ();
				}
			}
		}

		public Provider GetProviderById (int providerId)
		{
			Provider provider = null;
			using (SqlConnection conn = Database.CreateConnection ())
			{
				using (SqlCommand cmd = Database.CreateDbCmd (conn))
				{
					cmd.CommandText = "SELECT * FROM provider WHERE id = @ID";
					cmd.Parameters.AddWithValue ("@ID", providerId);

					using (var reader = cmd.ExecuteReader ())
					{
						if (reader.Read ())
						{
							provider = PopulateProvider (reader, true);
						}
					}
				}

				if (provider != null)
				{
					SetProviderTags (provider, conn);
					provider.Profile = GetProfile (providerId, conn);
				}
			}

			return provider;
		}

		/*private void GetImages(Profile profile, int providerID, SqlConnection conn)
		{
			using (SqlCommand cmd = Database.CreateDbCmd(conn))
			{
				cmd.CommandText = "SELECT image_id FROM image WHERE provider_id = @provider_id";
				cmd.Parameters.AddWithValue("@provider_id", providerID);

				using (var reader = cmd.ExecuteReader())
				{
					while (reader.Read())
					{
						profile.Images.Add(reader.GetString(0));
					}
				}
			}
		}*/

		public Profile GetProfile (int providerID)
		{
			using (SqlConnection conn = Database.CreateConnection ())
			{

				return GetProfile (providerID, conn);
			}
		}

		private Profile GetProfile (int providerID, SqlConnection conn)
		{
			var profile = new Profile ();

			using (SqlCommand cmd = Database.CreateDbCmd (conn))
			{
				cmd.CommandText = "SELECT profile_id, folder, bg_color, font_color, html, banner, images FROM profile WHERE provider_id = @provider_id";
				cmd.Parameters.AddWithValue ("@provider_id", providerID);

				using (var reader = cmd.ExecuteReader ())
				{
					if (reader.Read ())
					{
						profile.ID = reader.GetInt (ProfileFields.ID);
						profile.BgColor = reader.GetString (ProfileFields.BgColor);
						profile.FontColor = reader.GetString (ProfileFields.FontColor);
						profile.Html = reader.GetString (ProfileFields.Html);
						profile.Banner = reader.GetString (ProfileFields.Banner);
						profile.Folder = reader.GetString (ProfileFields.Folder);
						string images = reader.GetString (ProfileFields.Images);
						if (!string.IsNullOrEmpty (images))
						{
							profile.Images = images.Split (';').ToList ();
						}
						else
						{
							profile.Images = new List<string> ();
						}
					}
				}
			}

			return profile;
		}

		private void SetProviderTags (Provider provider, SqlConnection conn)
		{
			using (SqlCommand cmd = Database.CreateDbCmd (conn))
			{
				cmd.CommandText = "SELECT name FROM tag WHERE provider_id = @provider_id";
				cmd.Parameters.AddWithValue ("@provider_id", provider.ID);

				using (var reader = cmd.ExecuteReader ())
				{
					provider.Tags = new List<string> ();
					while (reader.Read ())
					{
						provider.Tags.Add (reader.GetString (0));
					}
				}
			}
		}

		public List<Provider> GetMyProviders (int userId)
		{
			List<Provider> result = new List<Provider> ();
			using (SqlConnection conn = Database.CreateConnection ())
			{
				using (SqlCommand cmd = Database.CreateDbCmd (conn))
				{
					cmd.CommandText = "SELECT p.* FROM provider p JOIN alist a on a.provider_id = p.id WHERE a.user_id = @user_id";
					cmd.Parameters.AddWithValue ("@user_id", userId);

					using (var reader = cmd.ExecuteReader ())
					{
						while (reader.Read ())
						{
							result.Add (PopulateProvider (reader, false));
						}
					}
				}

				if (result.Count > 0)
				{
					SetTags (result, conn);
				}
			}

			return result;
		}

		public List<Provider> GetProviders (string tag, float xLoc, float yLoc, SortBy sortBy, int? page, int? perPage)
		{
			float minX = xLoc - 1;
			float maxX = xLoc + 1;
			float minY = yLoc - 1;
			float maxY = yLoc + 1;

			List<Provider> result = new List<Provider> ();
			using (SqlConnection conn = Database.CreateConnection ())
			{
				using (SqlCommand cmd = Database.CreateDbCmd (conn))
				{
					cmd.CommandText = "SELECT DISTINCT p.* FROM provider p LEFT JOIN tag t on t.provider_id = p.id";
					cmd.CommandText += " WHERE (xcoord >= @minX AND xcoord <= @maxX AND ycoord >= @minY AND ycoord <= @maxY)";
					cmd.Parameters.AddWithValue ("@minX", minX);
					cmd.Parameters.AddWithValue ("@maxX", maxX);
					cmd.Parameters.AddWithValue ("@minY", minY);
					cmd.Parameters.AddWithValue ("@maxY", maxY);

					if (!string.IsNullOrEmpty (tag))
					{
						cmd.CommandText += string.Format (" AND t.name LIKE '{0}%'", tag);
					}

					using (var reader = cmd.ExecuteReader ())
					{
						while (reader.Read ())
						{
							result.Add (PopulateProvider (reader, false));
						}
					}
				}

				if (result.Count > 0)
				{
					SetTags (result, conn);
				}
			}

			return result;
		}

		private void SetTags (List<Provider> providers, SqlConnection conn)
		{
			List<int> ids = new List<int> ();
			Dictionary<int, Provider> map = new Dictionary<int, Provider> ();
			providers.ForEach (p =>
			{
				ids.Add (p.ID);
				map.Add (p.ID, p);
				p.Tags = new List<string> ();
			});

			using (SqlCommand cmd = Database.CreateDbCmd (conn))
			{
				cmd.CommandText = string.Format ("SELECT provider_id, name FROM tag WHERE provider_id in ({0})", string.Join (",", ids));
				using (var reader = cmd.ExecuteReader ())
				{
					while (reader.Read ())
					{
						int id = reader.GetInt (TagFields.ProviderID);
						map[id].Tags.Add (reader.GetString (TagFields.Name));
					}
				}
			}
		}

		private Provider PopulateProvider (SqlDataReader reader, bool withContent)
		{
			Provider result = new Provider ();
			result.ID = reader.GetInt (ProviderFields.ID);
			result.Name = reader.GetString (ProviderFields.Name);
			result.City = reader.GetString (ProviderFields.City);
			result.State = reader.GetString (ProviderFields.State);
			result.Zip = reader.GetString (ProviderFields.Zip);
			result.Website = reader.GetString (ProviderFields.Website);
			result.Phone = reader.GetString (ProviderFields.Phone);
			result.Referrals = reader.GetInt (ProviderFields.Referrals);
			result.XCoord = reader.GetFloat (ProviderFields.XCoord);
			result.YCoord = reader.GetFloat (ProviderFields.YCoord);
			result.OwnerId = reader.GetNullInt (ProviderFields.UserID);

			return result;
		}

		public void DeleteProvider (int providerId)
		{
			Database.DeleteWhere (AListFields.Table, AListFields.ProviderID, providerId);
			Database.DeleteWhere (ImageFields.Table, ImageFields.ProviderID, providerId);
			Database.DeleteWhere (ProfileFields.Table, ProfileFields.ProviderID, providerId);
			Database.DeleteWhere (TagFields.Table, TagFields.ProviderID, providerId);
			Database.DeleteWhere (ProviderFields.Table, ProviderFields.ID, providerId);
		}
	}
}