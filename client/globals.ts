interface IUser {
	name: string;
	email: string;
}

interface IError {
	errors: string[];
	success: boolean;
}

enum SortBy {
	Default = 'default',
	Referrals = 'referrals',
	Availability = 'Availability'
}

enum Pages {
	Home = '/',
	Search = '/results',
	Provider = '/provider',
	EditProvider = '/edit/provider',
	MyProviders = '/mine',
	Account = '/account'
}

interface ISignInModel {
	username: string;
	name: string;
	id: number;
	isAdmin?: boolean;
	bookmarks:Provider[];
}

interface IConfig {
	version: number;
	isProd: boolean;
	wssUrl: string;
}

declare var config: IConfig;
declare var gtag;
//declare var VueRouter;

declare var VueComponent: any;
declare var VueClassComponent;
var VueComponent: any = VueClassComponent.default;

VueComponent.registerHooks([
	'beforeRouteEnter',
	'beforeRouteLeave',
	'beforeRouteUpdate' // for vue-router 2.2+
])