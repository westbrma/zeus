class Tool {
	toolbox: Toolbox;
	name: string;
	editor:WYSIWYG;
	icon = null;
	active = false;

	constructor(name, editor:WYSIWYG) {
		this.name = name;
		this.editor = editor;
	}

	get quill():Quill {
		return this.editor.quill;
	}

	activate() {

	}

	setActive(el, format) {

	}
}

class HeaderTool extends Tool {
	number: number;

	constructor(headerNumber, editor:WYSIWYG) {
		super(headerNumber > 0 ? 'H' + headerNumber : 'P', editor);
		this.number = headerNumber;
	}

	activate() {
		if (this.active) {
			this.quill.format('header', false);
		} else {
			this.quill.format('header', this.number);
		}
	}

	setActive(el, format) {
		if (this.number == 0) {
			this.active = format.header == undefined;
		} else {
			this.active = format.header == this.number;
		}
	}
}

class Align extends Tool {
	cmd: string;
	textAlign: string;

	constructor(name, cmd, icon, editor:WYSIWYG, textAlign) {
		super(name, editor);
		this.cmd = cmd;
		this.icon = icon;
		this.textAlign = textAlign;
	}

	activate() {
		if (this.textAlign == 'left') {
			this.quill.format('align', false);
		} else {
			this.quill.format('align', this.textAlign);
		}
	}

	setActive(el, format) {
		this.active = format.align == this.textAlign;
		if (this.textAlign == 'left' && !this.active) {
			this.active = format.align == undefined;
		}
	}
}

class Undo extends Tool {
	constructor(editor:WYSIWYG) {
		super('Undo', editor);
		this.icon = 'undo';
	}

	activate() {
		this.quill.history.undo();
	}
}

class Redo extends Tool {
	constructor(editor:WYSIWYG) {
		super('Redo', editor);
		this.icon = 'redo';
	}

	activate() {
		this.quill.history.redo();
	}
}

class Bold extends Tool {
	constructor(editor:WYSIWYG) {
		super('Bold', editor);
		this.icon = 'format_bold';
	}

	activate() {
		this.quill.format('bold', !this.active);
	}

	setActive(el, format) {
		this.active = format.bold == true;
	}
}

class Underline extends Tool {
	constructor(editor:WYSIWYG) {
		super('Underline', editor);
		this.icon = 'format_underlined';
	}

	activate() {
		this.quill.format('underline', !this.active);
	}

	setActive(el, format) {
		this.active = format.underline == true;
	}
}

class Italic extends Tool {
	constructor(editor:WYSIWYG) {
		super('Italic', editor);
		this.icon = 'format_italic';
	}

	activate() {
		this.quill.format('italic', !this.active);
	}

	setActive(el, format) {
		this.active = format.italic == true;
	}
}

class Video extends Tool {
	video: string;

	constructor(editor:WYSIWYG) {
		super('Video', editor);
		this.icon = 'videocam';
		this.video = null;
	}

	activate() {
		this.quill.format('video', 'https://youtu.be/jpkDEpJyWpk');
	}

	setActive(el, format) {
		this.video = format.video;
		this.active = format.video != undefined;
	}
}

class Link extends Tool {
	link: string;

	constructor(editor:WYSIWYG) {
		super('Hyperlink', editor);
		this.icon = 'link';
		this.link = null;
	}

	activate() {
		var text = this.editor.saveSelection();
		if (text == null || text.length == 0) {
			this.editor.selectNode();
			this.editor.saveSelection();
		}

		LinkDlg.open(this.link).then(newLink => {
			this.editor.restoreSelection();
			this.quill.format('link', newLink);
		}).catch(() => {
			this.editor.restoreSelection();
			this.quill.format('link', null);
		});
	}

	setActive(el, format) {
		this.link = format.link;
		this.active = format.link != undefined;
	}
}

class ImageTool extends Tool {
	fileInput;
	insertIndex: number;

	constructor(editor:WYSIWYG) {
		super('Insert Image', editor);
		this.icon = 'insert_photo';
		this.fileInput = null;
		this.insertIndex = 0;
	}

	insertImage() {
		var file = this.fileInput[0].files[0];
		var reader = new FileReader();
		var quill = this.quill;

		reader.addEventListener("load", () => {
			quill.insertEmbed(this.insertIndex, 'image', reader.result);
			this.fileInput.val('');
		}, false);

		if (file) {
			reader.readAsDataURL(file);
		}
	}

	activate() {
		this.insertIndex = this.editor.getCaretIndex();

		if (this.fileInput == null) {
			this.fileInput = $('#imgupload');
			this.fileInput.on('change', this.insertImage.bind(this));
		}
		this.fileInput.trigger('click');
	}
}