Vue.directive("dropDownMenu", {
    bind: function (el, binding, vnode) {
        new DropDownMenu(el, binding, vnode);
    },
    componentUpdated: function (el:any, binding, vnode) {
        if(el.menu){
            el.menu.update(binding, vnode);
        }else{
            el.menu = new DropDownMenu(el, binding, vnode);
        }
    }
});

function DropDownMenu(el, binding, vnode) {
	var context = null;
	var clickCount = 0;
	doUpdate(binding, vnode);
	this.update = doUpdate;

    function doUpdate(binding, vnode) {
		if(context != vnode.context){
			context = vnode.context;
			context.toggleMenu = toggleMenu;
		}
	}
	
	function toggleMenu(){
		context.isMenuOpen = !context.isMenuOpen;
		
		if(context.isMenuOpen){
			$(document).on('click touchstart', onClick);
			clickCount = 0;
		}else{
			$(document).off('click touchstart', onClick);
		}
	}

	function onClick(event){
		event.stopPropagation();
		if(context.isMenuOpen){
			clickCount++;
			if(clickCount > 1){
				toggleMenu();
			}
		}
	}
}