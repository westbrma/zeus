USE [zeus]
GO

/****** Object:  Table [dbo].[user]    Script Date: 5/22/2018 5:05:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [nvarchar](80) NOT NULL,
	[password_hash] [nvarchar](80) NOT NULL,
	[last_session] [datetime] NULL,
	[name] [nvarchar](100) NOT NULL,
	[create_date] [datetime] NOT NULL,
	[last_login_date] [datetime] NOT NULL,
	[roles] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


