@VueComponent({
	template: `
	<div class="dialog" id="locationDlg">
		<div class="dlgrow">
			<label>ZIP Code</label>
			<input v-model="zip" />
		</div>
		<div class="footer">
			<button @click="close()">OK</button>
		</div>
	</div>`
})
class LocationDlg extends Vue {
	zip:string = null;

	static open(){
		let vm = new LocationDlg();
		vm.zip = state.location.zip;
		dlg.open(vm);
	}

	async close() {
		if (this.zip) {
			state.location.zip = this.zip;
			state.location.city = null;
			state.location.state = null;
			await locationService.setCoords(state.location);
		}
		dlg.close(this);
	}
}