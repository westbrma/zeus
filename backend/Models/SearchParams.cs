using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeus
{
	public class SearchParams
	{
		public string[] Tags;
		public SortBy SortBy;
		public string TitleSearch;
		public int? AuthorID;
		public int? Page;
		public int? PerPage;
	}
}