using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zeus
{
	public static class Database
	{
		public static readonly string ConnString;

		static Database ()
		{
			if (Helper.IsDebug)
			{
				ConnString = @"Data Source=Mark-PC\SQLExpress;Initial Catalog=zeus;Integrated Security=True";
			}
			else
			{
				ConnString = @"Data Source=sql5004.site4now.net;Initial Catalog=DB_9FDBD9_zeus;User Id=DB_9FDBD9_zeus_admin;Password=P@ssw0rd;";
			}
		}

		static Random randGen = new Random ();
		static DateTime mNextMinTime = DateTime.UtcNow;
		static int mNextKey = 100000 + randGen.Next (1, 1000);

		public static long GetPrimaryKey ()
		{
			DateTime now = DateTime.UtcNow;

			// safety to ensure the number is at least one minute more than the last reset
			if (now < mNextMinTime)
			{
				now = mNextMinTime;
			}

			mNextKey += randGen.Next (1, 1000);
			if (mNextKey >= 1000000) // reset if 7 digits reached, which will be happen about every 1800 inserts
			{
				mNextKey = 100000 + randGen.Next (1, 1000);
				mNextMinTime = new DateTime (now.Year, now.Month, now.Day, now.Hour, now.Minute, 0);
				mNextMinTime = mNextMinTime.AddMinutes (1);
			}

			string keyStr = now.ToString ("yyMMddHHmm") + mNextKey;
			return Convert.ToInt64 (keyStr);
		}

		public static SqlConnection CreateConnection ()
		{
			var conn = new SqlConnection (ConnString);
			conn.Open ();
			return conn;
		}

		public static SqlCommand CreateDbCmd (string query, SqlConnection conn = null, SqlTransaction transaction = null)
		{
			return new SqlCommand (query, conn, transaction);
		}

		public static SqlCommand CreateDbCmd (SqlConnection conn = null, SqlTransaction transaction = null)
		{
			return new SqlCommand (null, conn, transaction);
		}

		public static void DeleteWhere (string table, string column, object columnValue)
		{
			using (SqlConnection conn = CreateConnection ())
			{
				using (SqlCommand cmd = CreateDbCmd (conn))
				{
					cmd.CommandText = string.Format ("DELETE FROM {0} WHERE {1} =  @ColumnValue", table, column);
					cmd.Parameters.AddWithValue ("@ColumnValue", columnValue);

					cmd.ExecuteNonQuery ();
				}
			}
		}

		public static void AddWhere (SqlCommand cmd, string column, string op, object value)
		{
			bool hasWhere = cmd.CommandText.IndexOf ("where") > 0;
			if (hasWhere)
			{
				cmd.CommandText += " and ";
			}
			else
			{
				cmd.CommandText += " where ";
			}

			string paramName = "@" + column;
			var idx = 0;
			foreach (SqlParameter param in cmd.Parameters)
			{
				if (param.ParameterName == paramName)
				{
					idx++;
					paramName = "@" + column + idx;
				}
			}

			cmd.CommandText += string.Format ("{0} {1} {2}", column, op, paramName);
			cmd.Parameters.AddWithValue (paramName, value);
		}
	}
}