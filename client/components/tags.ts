@VueComponent({
	template: templates.fetch('tags'),
	props:['provider']
})
class Tags extends Vue {
	state = state;
	provider:Provider;
	tags:string[] = [];
	allTags:string[] = [];
	newTag:string = null;
	showOptions:boolean = false;

	created(){
		this.tags = this.provider.tags;
		this.allTags = ['electrician', 'plumber', 'cleaner'];
	}

	get tagList():string[] {
		if(!this.newTag){
			return this.allTags;
		}
		
		return this.allTags.filter(t => t.indexOf(this.newTag) >= 0);
	}

	openDetail(story:Provider) {
		router.push('/article/' + story.id);
	}

	removeTag(tag:string){
		removeFromArray(tag, this.tags);
		this.focus();
	}

	focus(){
		this.$el.querySelector('input').focus();
	}

	addTag(tag:string){
		if(!tag){
			return;
		}
		
		if(this.tags.indexOf(tag) < 0){
			this.tags.push(tag);
		}

		this.newTag = null;
		this.focus();
	}
}

Vue.component('tags', Tags);