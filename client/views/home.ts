@VueComponent({
	template: templates.fetch('home')
})
class Home extends Vue {
	state = state;
	commonTags:string[] = ['Electrician','Handyman','Plumber','Cleaning','Dump Runner','Carpenter'];
	commonTags2:string[] = ['House Cleaning','Windows and Doors','Flooring','Pavement and Concrete','Landscaping'];

	created(){
		state.onHome = true;
	}

	beforeRouteLeave(to, from, next) {
		state.onHome = false;
		next();
	}
	
	addNewProvider(){
		app.goto(Pages.EditProvider);
	}

	async search(tag:string){
		state.items = await providerService.getProviders(tag);
		app.goto(Pages.Search);
	}
}