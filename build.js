const gulp = require('gulp'),
	runSequence = require('run-sequence'),
	del = require('del'),
	watch = require('gulp-watch'),
	replace = require('gulp-replace'),
	md5 = require('md5'),
	foreach = require('gulp-foreach'),
	fs = require('fs'),
	path = require('path'),
	through = require('through2'),
	argv = require('yargs').argv,
	gulpif = require('gulp-if'),
	uglify = require('gulp-uglify-es').default,
	gutil = require('gulp-util'),
	sass = require('gulp-sass'),
	ts = require('gulp-typescript'),
	sourcemaps = require('gulp-sourcemaps');


var buildOnly = !!argv.buildOnly;
console.log('buildOnly:'+ buildOnly);
var prod = !!argv.prod;
var buildDir = './backend/wwwroot';
var includes = [];
var templates = {};
var vendorIncludes = [];
var guiVersion = getVersion();

var tsArgs = {};
tsArgs.outFile = "app.js";
tsArgs.inlineSourceMap = true;

if(prod){
	tsArgs.inlineSourceMap = undefined;
}

var tsProject = ts.createProject("./client/tsconfig.json", tsArgs);

var paths = {
	index: 'client/index.html',
	templates: 'client/helpers/templates.ts',
	html: ['client/**/*.html'],
	assets: ['client/**/*.jpg', 'client/**/*.png', 'client/**/*.woff', 'client/**/*.wav'],
	sass: ['client/css/styles.scss'],
	sassAll: ['client/**/*.scss'],
	js: [],
	ts: [
		"client/globals.ts",
		"client/helpers/*.ts",
		"client/models/*.ts",
		"Client/state.ts",
		"client/services/*.ts",
		"client/wysiwyg/toolbox.ts",
		"client/wysiwyg/*.ts",
		"client/views/*.ts",
		"client/router.ts",
		"client/**/*.ts"
	],
	tsjs: [],
	vendorJS: [
		'node_modules/axios/dist/axios.min.js',
		'node_modules/vue/dist/vue.js',
		'node_modules/vue-class-component/dist/vue-class-component.min.js',
		'node_modules/vue-router/dist/vue-router.min.js',
		'node_modules/jquery/dist/jquery.min.js'
	]
}

paths.tsjs = paths.js.concat(paths.ts);

// switch to min vendor files for prod
if (prod) {
	useMinFile('node_modules/vue/dist/vue.js', 'node_modules/vue/dist/vue.min.js');
}

function useMinFile(file, minFile) {
	var idx = paths.vendorJS.indexOf(file);
	paths.vendorJS.splice(idx, 1, minFile);
}

function getVersion() {
	var verFile = 'client/version.txt';
	var version = fs.readFileSync(verFile).toString();
	if (prod) {
		version = (parseInt(version) + 1).toString();
		fs.writeFile(verFile, version);
	}

	console.log("version:" + version);
	return version;
}

function addIncludeFile(file, appendHash) {
	if (appendHash) {
		var buf = fs.readFileSync(file).toString();
		file += '?v=' + md5(buf);
	}

	var line = '<script src="' + file + '"></script>';
	//console.log(line);
	includes.push(line);
}

function runTask(task) {
	return through.obj(function (file, enc, cb) {
		gulp.start(task);
		cb(null, file);
	});
}

function includeFiles(appendHash, list, prefix) {
	return through.obj(function (file, enc, cb) {
		var idx = file.path.lastIndexOf('.');
		var ext = file.path.substring(idx + 1).toLowerCase();

		var fromPath = path.join(file.cwd, file.base);
		if (file.base.indexOf(':') >= 0) {
			fromPath = file.base;
		}
		//console.log(file.path + ' | cwd:' + file.cwd + ' | base: ' + file.base + ' | ' + fromPath);

		var relPath = path.relative(fromPath, file.path)
		if (appendHash) {
			relPath += '?v=' + md5(file.contents);
		}

		if (prefix) {
			relPath = prefix + relPath;
		}
		relPath = '/' + relPath.replace(/\\/g, "/");

		var line = '';
		if (ext == 'js') {
			line = '<script src="' + relPath + '"></script>';
		} else if (ext == 'css') {
			line = '<link rel="stylesheet" href="' + relPath + '" />';
		}

		if (list.indexOf(line) < 0) {
			list.push(line);
		}
		cb(null, file);
	});
}

function genTemplate() {
	return through.obj(function (file, enc, cb) {
		var name = path.basename(file.path, '.html');
		templates[name] = new Buffer(file.contents).toString();
		console.log('template:'+name);
		cb(null, file);
	});
}

gulp.task('clean', (cb) => del(buildDir, cb));

gulp.task('vendorJS', function () {
	return gulp.src(paths.vendorJS, { base: './node_modules/' })
		.pipe(includeFiles(prod, vendorIncludes, 'vendor/'))
		.pipe(gulp.dest(buildDir + '/vendor'));
});

gulp.task('ts', function () {
	const tsResult = gulp.src(paths.tsjs, { base: './client/' })
		.pipe(sourcemaps.init())
		.pipe(tsProject());

	//.pipe(gulp.dest(buildDir))

	return tsResult.js
		// .pipe(gulpif(prod, uglify().on('error', function (err) {
		// 	console.log(err);
		// })))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(buildDir))
		.pipe(includeFiles(prod, includes));
});

gulp.task('sass', function () {
	return gulp.src(paths.sass, { base: './client/' })
		.pipe(sass().on('error', sass.logError))
		.pipe(includeFiles(prod, includes))
		.pipe(gulp.dest(buildDir));
});

gulp.task('html', async function (cb) {
	console.log('starting html');

	await new Promise(resolve => {
		gulp.src(paths.html)
			.pipe(genTemplate())
			.on('end', resolve)
			.on('data', ()=>{});
	});

	var lines = "_cache = " + JSON.stringify(templates);
	fs.writeFileSync(buildDir+'/templates.js', lines);
	return cb;
});

gulp.task('assets', function () {
	return gulp.src(paths.assets)
		.pipe(gulp.dest(buildDir));
});

gulp.task('index', function () {
	var jsLines = '\t' + includes.join('\n\t');
	var vendorLines = '\t' + vendorIncludes.join('\n\t');

	gulp.src(paths.index)
		.pipe(replace('<!-- VENDOR_FILES -->', vendorLines))
		.pipe(replace('<!-- JS_FILES -->', jsLines))
		.pipe(replace('"<!-- VERSION -->"', guiVersion))
		.pipe(replace('"<!-- PROD -->"', prod))
		.pipe(gulp.dest(buildDir));
});

gulp.task('build', function (cb) {
	runSequence('clean', ['sass', 'assets', 'html', 'ts', 'vendorJS'], 'index', cb);
});

var watchTsProject = ts.createProject("./client/tsconfig.json", {
	isolatedModules: true
});

gulp.task('default', ['build'], function () {
	console.log('watching for changes..');
	watch(paths.sassAll, function (file) {
		console.log(file.path);
		gulp.start('sass');
	});

	gulp.watch('client/index.html', ['index']);
	gulp.watch(paths.html, ['html']);
	gulp.watch(paths.tsjs, ['ts']);
});

if (buildOnly){
	gulp.start('build');
}else{
	gulp.start('default');
}