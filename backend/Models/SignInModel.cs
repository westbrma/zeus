using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Zeus
{
	public class SignInModel
	{
		public string Username { get; set; }
		public string Name { get; set; }
		public int ID { get; set; }
		public bool IsAdmin { get; set; }
		public IEnumerable<Provider> Bookmarks { get; set; }
	}
}