USE [zeus]
GO

/****** Object:  Table [dbo].[alist]    Script Date: 5/8/2018 5:55:26 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[alist](
	[user_id] [int] NOT NULL,
	[provider_id] [int] NOT NULL,
 CONSTRAINT [PK_alist] PRIMARY KEY CLUSTERED 
(
	[user_id] ASC,
	[provider_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[alist]  WITH CHECK ADD  CONSTRAINT [FK_alist_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([id])
GO

ALTER TABLE [dbo].[alist] CHECK CONSTRAINT [FK_alist_provider_id]
GO

ALTER TABLE [dbo].[alist]  WITH CHECK ADD  CONSTRAINT [FK_alist_user_id] FOREIGN KEY([user_id])
REFERENCES [dbo].[user] ([id])
GO

ALTER TABLE [dbo].[alist] CHECK CONSTRAINT [FK_alist_user_id]
GO


