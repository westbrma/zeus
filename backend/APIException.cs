using System;
using System.Net;

namespace Zeus
{
	public class APIException : Exception
	{
		public int StatusCode;

		public APIException (string message, HttpStatusCode statusCode = HttpStatusCode.BadGateway) : base (message)
		{
			StatusCode = (int) statusCode;
		}
	}
}