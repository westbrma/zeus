@VueComponent({
	template: `
	<div class="dialog">
		<div>{{message}}</div>
		<div class="footer">
			<button @click="close(true)">OK</button>
			<button @click="close()">Cancel</button>
		</div>
	</div>`
})
class ConfirmDlg extends Vue {
	message:string = '';
	defer:Defer<boolean> = new Defer();

	static open(message:string):Promise<boolean>{
		let vm = new ConfirmDlg();
		vm.message = message;
		dlg.open(vm);
		return vm.defer.promise;
	}

	close(saved:boolean = false){
		dlg.close(this);
		this.defer.return(true, saved);
	}
}