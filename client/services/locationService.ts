interface IIPInfo {
	"ip": string;
	"hostname": string;
	"city": string;
	"region": string;
	"country": string;
	"loc": string; // "43.6320,-116.2840",
	"postal": string;
	"org": string;
}

const apiKey = 'AIzaSyBMFu11aBFPTFcSdl-N_LES2hKvzE4rPUA';

class LocationService {
	fetch() {
		$.get("http://ipinfo.io", function (response: IIPInfo) {
			state.location.city = response.city;
			state.location.state = response.region;
			state.location.zip = response.postal;
			let coords = response.loc.split(',');
			state.location.xcoord = parseFloat(coords[1]);
			state.location.ycoord = parseFloat(coords[0]);
		}, "jsonp");
	}

	async setCoords(location: UserLocation) {

		function getResultItem(result, type:string):string{
			let found = result.address_components.find(c => c.types.indexOf(type) >= 0);
			
			if(found) {
				return found.long_name;
			}
		}

		let str = '';
		if (location.address) {
			var words = location.address.split(' ');
			words.forEach(function (w) {
				str += '+' + w;
			});
		}

		if(str){
			str += ',+';	
		}
		if(location.city && location.state){
			str += location.city + ',+' + location.state;
		}else if(location.zip){
			str += location.zip;
		}

		let response = await $http.get('https://maps.googleapis.com/maps/api/geocode/json?address=' + str + '&key=' + apiKey);
		let data = response.data;

		if (data.results.length > 0) {
			let result = data.results[0];

			if(!location.city){
				location.city = getResultItem(result, 'locality');
			}

			if(!location.state){
				location.state = getResultItem(result, 'administrative_area_level_1');
			}

			if (result.geometry != null) {
				location.xcoord = result.geometry.location.lng;
				location.ycoord = result.geometry.location.lat;
			}
		}
	}
}

var locationService = new LocationService();