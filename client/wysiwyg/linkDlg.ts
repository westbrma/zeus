@VueComponent({
	template: templates.fetch('linkDlg')
})
class LinkDlg extends Vue {
	link = '';
	defer: Defer<boolean> = new Defer();

	static open(link) {
		let vm = new LinkDlg();
		vm.link = link;
		dlg.open(vm);
		return vm.defer.promise;
	}

	preview() {

	}

	save() {
		this.close(true);
	}

	close(saved) {
		this.defer.return(saved, this.link);
		dlg.close(this);
	}
}