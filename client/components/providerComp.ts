@VueComponent({
	props:['provider'],
	template:
	`<div class="provider" @click="openDetail()">
		<div>
			<span class="name">{{provider.name}}</span>
			<span>{{tags}}</span>
		</div>
		<div>
			<span v-if="provider.city">City: <b>{{provider.city}}</b></span>
			<span v-if="provider.phone">Phone: <b>{{provider.phone}}</b></span>
			<span v-if="provider.website">Website: <a>{{provider.website}}</a></span>
		</div>
		<div>
			<span><b>{{provider.referrals}}</b> referrals</span>
			<span><b>2</b> comments</span>
		</div>
	</div>`
})
class ProviderComp extends Vue {
	provider:Provider;

	openDetail() {
		app.goto(Pages.Provider + '/' + this.provider.id)
	}

	get tags():string {
		return this.provider.tags.join(', ');
	}
}

Vue.component('provider', ProviderComp);