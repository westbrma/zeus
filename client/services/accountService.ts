class AccountService {
	tryLogin(model:ISignInModel) {
		if (model) {
			state.signedIn = true;
			state.account = model;
			state.bookmarks = model.bookmarks;
		}
	}

	login(username, password) {
		return $http.post('/api/user/login', {
			username: username,
			password: password
		}).then(r => {
			this.tryLogin(r.data);
		});
	}

	logout() {
		return $http.post('/api/user/SignOut', null).then(() => {
			state.signedIn = false;
			state.account = null;
			app.goto(Pages.Home);
		});
	}

	getLogin() {
		return state.signInPromise = $http.get('/api/user').then(r => {
			this.tryLogin(r.data);
		});
	}

	resetPassword(username) {
		return $http.post('/api/user/ForgotPassword', {
			'email': username
		});
	}

	register(username, password, name) {
		return $http.post('/api/user/register', {
			username: username,
			password: password,
			name: name
		}).then((response) => {
			this.tryLogin(response);
		});
	}

	updateAccount(username, password, name) {
		var data = {
			'username': username,
			'password': password,
			'name': name
		}

		return $http.put('/api/user', data).then(function () {
			state.account.name = name;
			state.account.username = username;
		});
	}
}

var accountService = new AccountService();