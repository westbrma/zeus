var hasGtag = typeof gtag !== 'undefined'

if(config.isProd && hasGtag){
	gtag('config', 'UA-114354000-1');
}

enum MetricAction {
	
}

function trackEvent(action: MetricAction, data?:any) {
	if (!config.isProd || !hasGtag) return;

	gtag('event', action, {
		'event_category': 'game',
		'value' : data
	});
}