var monthNames = [
	"January", "February", "March",
	"April", "May", "June", "July",
	"August", "September", "October",
	"November", "December"
];

function formatDate(dateStr) {
	if(dateStr == null || dateStr == ''){
		return '';
	}

	var date = new Date(dateStr);

	var day = date.getDate();
	var monthIndex = date.getMonth();
	var year = date.getFullYear();

	return monthNames[monthIndex] + ' ' + day + ' ' + year;
}

Vue.filter('formatDate', function(value) {
  return formatDate(value);
});

Vue.filter('tagString', function(tags) {
  return tags.join(', ');
});