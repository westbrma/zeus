declare var _cache: { [key: string]: string };
// HTML_TEMPLATES

class Templates {
	fetch(file: string): string {
		return _cache[file];
	}
}

var templates = new Templates();