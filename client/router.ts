const routes:any = [
	{
		path: '/',
		components: {
			default: Home
		}
	},
	{
		path: '/faq',
		components: {
			default: FAQ
		}
	},
	{
		path: '/results',
		components: {
			default: SearchResults,
			navbar: DefaultNavbar
		}
	},
	{
		path: '/account',
		components: {
			default: Accounts,
			navbar: DefaultNavbar
		}
	},
	{
		path: '/provider/:id',
		components: {
			default: ProviderView,
			navbar: ProviderBar
		}
	},
	{ 
		path: '/edit/provider/:id?', 
		components: {
			default: EditProvider,
			navbar: EditProviderBar
		}
	},
	{
		path: '/mine',
		components: {
			default: Mine,
			navbar: DefaultNavbar
		}
	},
	{
		path: '/admin',
		components: {
			default: Admin,
			navbar: AdminBar
		}
	}
]

const router = new VueRouter({
	mode: 'history',
	routes
});