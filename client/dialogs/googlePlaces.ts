@VueComponent({
	template: templates.fetch('googlePlaces')
})
class GooglePlacesDlg extends Vue {
	result:IGoogleResult = null;
	latitude:number = null;
	longitude:number = null;
	placeType:string = null;

	static open() {
		let vm = new GooglePlacesDlg();
		dlg.open(vm);
	}

	async search(){
		this.result = await adminService.googleSearch(this.latitude, this.longitude, this.placeType);
	}

	close() {
		dlg.close(this);
	}
}