interface IGoogleResult {
	next_page_token: string;
	results: IGooglePlace[];
}

interface IGoogleLocation {
	lat: string;
	long: string;
}

interface IGoogleGeometry {
	location: IGoogleLocation;
}

interface IGooglePlace {
	geometry: IGoogleGeometry;
	id: string;
	name: string;
	place_id: string;
	rating: string;
	reference: string;
	vicinity: string;
}

class AdminService {
	getUsers = function (): Promise<IUser[]> {
		return $http.get('/api/admin/users').then(r => {
			return r.data
		});
	}

	googleSearch(lat: number, long: number, type: string, nextPageToken?:string):Promise<IGoogleResult> {

		let baseURL = 'https://maps.googleapis.com/maps/api/place/nearbysearch/json';
		let params: URLSearchParams = new URLSearchParams();
		params.append('radius', '50000');
		params.append('key', apiKey);
		params.append('type', type);
		params.append('location', `${lat},${long}`);

		if (nextPageToken) {
			params.append('pagetoken', nextPageToken);
		}

		return $http.get(baseURL + '?' + params.toString()).then(r => {
			return r.data as IGoogleResult
		});
	}
}

let adminService = new AdminService();