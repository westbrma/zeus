class HttpService {
	tryShowError(response) {
		if (response.response) {
			response = response.response;
		}

		if (response.data && response.data.errors) {
			ErrorDlg.open(response.data.errors);
		}
	}

	post(url:string, data:any, config?:AxiosConfig):Promise<any>{
		return axios.post(url, data, config);
	}

	put(url:string, data:any, config?:AxiosConfig):Promise<any>{
		return axios.put(url, data, config);
	}

	delete(url:string, config?:AxiosConfig):Promise<any>{
		return axios.delete(url, config);
	}

	get(url:string, config?:AxiosConfig):Promise<any>{
		return axios.get(url, config);
	}
}

let $http = new HttpService();

function onResponse(response) {
	$http.tryShowError(response);
	return response;
}

function onError(error) {
	$http.tryShowError(error);
	return Promise.reject(error);
}

// Add a response interceptor
axios.interceptors.response.use(onResponse, onError);

axios.interceptors.request.use(function (config) {
    // Do something before request is sent
    return config;
  }, function (error) {
    // Do something with request error
    return Promise.reject(error);
  });