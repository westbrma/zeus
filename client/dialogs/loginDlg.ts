@VueComponent({
	template: templates.fetch('loginDlg')
})
class LoginDlg extends Vue {
	username = '';
	password = '';
	error = '';

	static open() {
		let vm = new LoginDlg();
		dlg.open(vm);
	}

	register() {
		this.close();
		RegisterDlg.open();
	}

	close() {
		dlg.close(this);
	}

	login() {
		accountService.login(this.username, this.password).then(this.close);
	}

	resetPassword() {
		if (this.username == '') {
			MessageDlg.open('Enter your email address first');
			(<HTMLElement>this.$refs.email).focus();
			return;
		}

		var self = this;
		accountService.resetPassword(this.username).then(function () {
			self.close();
			MessageDlg.open("An email has been sent with instructions on recovering your account");
		});
	}
}