@VueComponent({
	props: ['editor'],
	components: {
		'tool': {
			props: ['tool'],
			template:
				`<div :title="tool.name" :class="{active:tool.active}" class="tool" @mousedown.prevent="tool.activate()">
				<i v-if="tool.icon" class="material-icons">{{tool.icon}}</i>
				<span v-else>{{tool.name}}</span>
			</div>`,
			methods: {
			}
		}
	},
	template:
		`<div id="toolbox">
		<div class="tools">
			<tool v-for="(tool, $index) in tools" :tool="tool" :key="$index"></tool>
		</div>
	</div>`,
})
class Toolbox extends Vue {
	enabled: boolean = true;
	editor: WYSIWYG;
	tools = [];

	created() {
		this.tools = [
			new HeaderTool(2, this.editor),
			new HeaderTool(3, this.editor),
			new Align('Align Left', 'justifyLeft', 'format_align_left', this.editor, 'left'),
			new Align('Align Right', 'justifyRight', 'format_align_right', this.editor, 'right'),
			new Align('Align Center', 'justifyCenter', 'format_align_center', this.editor, 'center'),
			new Undo(this.editor),
			new Redo(this.editor),
			new ImageTool(this.editor),
			new Link(this.editor),
			new Bold(this.editor),
			new Italic(this.editor),
			new Underline(this.editor)
		]
	}
}