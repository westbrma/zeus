class State {
	onHome:boolean = false;
	provider: Provider = null;
	items:Provider[] = [];
	signedIn = false;
	account:ISignInModel = {
		username: '',
		name: '',
		id: 0,
		isAdmin: false,
		bookmarks:[]
	};
	bookmarks:Provider[] = [];
	location:UserLocation = new UserLocation();
	signInPromise:Promise<any>;
}

var state = new State();