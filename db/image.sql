USE [zeus]
GO

/****** Object:  Table [dbo].[image]    Script Date: 5/21/2018 6:23:27 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[image](
	[image_id] [varchar](50) NOT NULL,
	[provider_id] [int] NOT NULL,
 CONSTRAINT [PK_image] PRIMARY KEY CLUSTERED 
(
	[image_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[image]  WITH CHECK ADD  CONSTRAINT [FK_image_provider_id] FOREIGN KEY([provider_id])
REFERENCES [dbo].[provider] ([id])
GO

ALTER TABLE [dbo].[image] CHECK CONSTRAINT [FK_image_provider_id]
GO


