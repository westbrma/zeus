@VueComponent({
	template: templates.fetch('header')
})
class Header extends Vue {
	state = state;
	app = app;
	isMenuOpen = false;

	get name():string{
		if(state.account.name){
			return state.account.name;
		}else{
			return state.account.username;
		}
	}

	login() {
		LoginDlg.open();
	}

	account(){
		router.push('/account');
	}

	logout(){
		accountService.logout();
	}

	toggleMenu(){
		this.isMenuOpen = true;
	}
}

Vue.component('my-header', Header);