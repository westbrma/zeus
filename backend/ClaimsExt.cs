using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Zeus
{
	public static class ClaimsExt
	{
		public static int UserID (this ClaimsPrincipal user)
		{
			var claim = user.FindFirst ("user_id");
			return int.Parse (claim.Value);
		}

		public static string Email (this ClaimsPrincipal user)
		{
			var claim = user.FindFirst (ClaimTypes.Name);
			return claim.Value;
		}

		public static User UserObj (this ClaimsPrincipal user)
		{
			return Repo.Users.GetUser (user.UserID ());
		}
	}
}