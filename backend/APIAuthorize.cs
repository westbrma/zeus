using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Zeus
{
	public class APIAuthorize : ActionFilterAttribute
	{
		static JsonSerializerSettings _jsonSettings = new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver () };
		UserRoles _roles;

		public APIAuthorize (UserRoles roles = UserRoles.None) : base ()
		{
			_roles = roles;
		}

		public override void OnActionExecuting (ActionExecutingContext context)
		{
			var user = context.HttpContext.User;
			if (!user.Identity.IsAuthenticated || (_roles != UserRoles.None && !user.UserObj ().IsRole (_roles)))
			{
				throw new APIException ("You are unauthorized for this request", HttpStatusCode.Unauthorized);
			}
			else
			{
				base.OnActionExecuting (context);
			}
		}
	}
}