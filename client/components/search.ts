@VueComponent({
	template: templates.fetch('search')
})
class Search extends Vue {
	state = state;
	selTags = [];
	searchText = '';
	sortBy = SortBy.Referrals;
	terms:string[] = [];
	showTerms:boolean = false;
	hlIndex:number = -1;

	created(){
		if (this.$route.query.search) {
			this.searchText = this.$route.query.search;
		}

		if (this.$route.query.tags) {
			if (!Array.isArray(this.$route.query.tags)) {
				this.selTags.push(this.$route.query.tags);
			} else {
				this.selTags = JSON.parse(JSON.stringify(this.$route.query.tags));
			}
		}

		this.$watch('searchText', ()=> {
			this.refreshTerms();
		});

		this.refreshTerms();
	}

	async refreshTerms() {
		this.terms = await providerService.getTags(this.searchText);
	}

	async search(term:string) {
		state.items = await providerService.getProviders(term);
		if(state.onHome){
			app.goto(Pages.Search);
		}
	}

	onKeyDown($event:KeyboardEvent){
		switch($event.keyCode){
			case 13:
				this.search(this.searchText);
				break;
			case 38:
				this.hlNextTerm(-1);
				break;
			case 40:
				this.hlNextTerm(1);
				break;
		}
	}

	hlNextTerm(dir:number){
		if(dir > 0){
			this.hlIndex++;
		}else{
			this.hlIndex--;
		}

		if(this.hlIndex < 0){
			this.hlIndex = 0;
		}else if(this.hlIndex >= this.terms.length){
			this.hlIndex = this.terms.length - 1;
		}

		//this.searchText = this.terms[this.hlIndex];
	}

	addTag(tag) {
		if (this.selTags.indexOf(tag) < 0) {
			this.selTags.push(tag);
		}
	}

	removeTag(tag) {
		var idx = this.selTags.indexOf(tag);
		if (idx >= 0) {
			this.selTags.splice(idx, 1);
		}
	}

	editLocation(){
		LocationDlg.open();
	}
}

Vue.component('search', Search);