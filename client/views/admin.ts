@VueComponent({
	template: templates.fetch('admin')
})
class Admin extends Vue {
	users = [];

	async created() {
		this.users = await adminService.getUsers();
	}
}